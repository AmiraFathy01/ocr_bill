import re
import regex as re
import pandas as pd
# reverse arabic words to be by the right order before work and let the english like it is without any changes
#in invoice code - reverse only arabic text
def reverse_arabic_words_inv(text):
    # Split text into lines
    lines = text.split('\n')
    result = []
    # Iterate through each line
    for line in lines:
        # Split line into words
        words = line.split()
        # Separate Arabic and non-Arabic words
        arabic_words = []
        non_arabic_words = []
        for word in words:
            # Check if the word contains Arabic characters
            if re.search(r'[\u0600-\u06FF]+', word):
                arabic_words.append(word)  # Collect Arabic words
            else:
                non_arabic_words.append(word)  # Collect non-Arabic words
        # Reverse order of Arabic words
        arabic_words_reversed = arabic_words[::-1]
        # Combine Arabic and non-Arabic words back into a line
        reversed_line = ' '.join(non_arabic_words + arabic_words_reversed)
        result.append(reversed_line)
    # Join the reversed lines back into a single text
    return '\n'.join(result)  

# in table code - reverse text and numbers
def reverse_arabic_words_tab(txt):
    # Check if text is NaN (empty) or a float
    if pd.isna(txt) or isinstance(txt, float) or isinstance(txt, int):
        return str(txt)
    # Split text into lines
    lines = txt.split('\n')
    result = []
    # Iterate through each line
    for line in lines:
        # Split line into words
        words = line.split()
        # Separate Arabic and non-Arabic words
        arabic_words = []
        non_arabic_words = []
        for word in words:
            # Check if the word contains Arabic characters
            if re.search(r'[\u0600-\u06FF0-9]+', word):
                arabic_words.append(word)  # Collect Arabic words including numbers
            else:
                non_arabic_words.append(word)  # Collect non-Arabic words
        # Reverse order of Arabic words
        arabic_words_reversed = arabic_words[::-1]
        # Combine Arabic and non-Arabic words back into a line
        reversed_line = ' '.join(non_arabic_words + arabic_words_reversed)
        result.append(reversed_line)
    # Join the reversed lines back into a single text
    return '\n'.join(result) 