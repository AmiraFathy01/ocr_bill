import re
import regex as re
# this function extract the currencies with the amount after it
# this function attempts to extract a numerical value from the input string text, handling cases where the number might have commas as thousands 
# separators or dots as decimal separators. It also rounds the extracted number to two decimal places before returning it.

#used in inv code
def convert_amount_format_inv(text):
    # Define patterns for different currencies
    # Construct the pattern with specific currency patterns
    #pattern = r'(?P<amount>\d+,?\d*\.?\d*\.?\d*?)\s*\b'
    # Extract currency and amount using the pattern
    #matches = re.finditer(pattern, text, re.IGNORECASE)
    # Initialize the dictionary to store currency and amount
    #currency_and_amount = {}
    #for match in matches:       
     #   amount_str = match.group('amount') 
      #  print(f"amount in conver { amount_str}")
    num= None
    amount_str=text
    if "," in amount_str:
    # Remove the comma and get the part after the comma
        num_after_comma_str = amount_str.split(",")[1]
        # Check if the part after the comma has 3 or more digits 
        if len(num_after_comma_str) >= 3:
            # if have 3 or more after comma will remove the comma 
            num = float(amount_str.replace(",", ""))
            #print(f"The number is {num} and has 3 or more digits after the comma.")
        # Check if the part after the comma has 2  digits 
        elif len(num_after_comma_str) == 2:
            # if have 2  will replace the comma , by dot .  
            num = float(amount_str.replace(",", "."))
            #print("The number has a comma but does not have 3 or more digits after the comma.")
    # Check if the amount have 2 or more dot . 
    elif amount_str.count('.')>=2:
            # Find the index of the first dot
            first_dot_index = amount_str.find('.')
            # Remove the first dot using slicing
            modified_amount_str = amount_str[:first_dot_index] + amount_str[first_dot_index+1:]
            num=float(modified_amount_str)
    else:
        # If no comma, assume the whole string is the number
        num = float(amount_str)
        #print(f"The number is {num} and does not have a comma.")            
    if num!= None:
        convert= round(num,2)
        return convert 
    return num         

#used in extract tables code
def convert_amount_format_tab(txt_num):
    num= None
    if isinstance(txt_num, str):
        parts = txt_num.split()  # Split the string based on spaces
        processed_parts = []
        for part in parts:
            if "," in part:
                num_after_comma_str = part.split(",")[1]
                if len(num_after_comma_str) >= 3:
                    num = part.replace(",", "")
                elif len(num_after_comma_str) == 2:
                    num = part.replace(",", ".")
            elif part.count('.') >= 2:
                first_dot_index = part.find('.')
                modified_amount_str = part[:first_dot_index] + part[first_dot_index+1:]
                num = modified_amount_str
            else:
                num = part
            if num is not None:
                processed_parts.append(str(num))
        return ' '.join(processed_parts)  # Join the processed parts with spaces
    elif isinstance(txt_num, float):
        return str(txt_num)
    else:
        return None