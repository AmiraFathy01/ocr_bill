from PIL import Image
import PIL
import numpy as np
import PIL.Image
import requests
import textwrap
import google.generativeai as genai
from IPython.display import display
from IPython.display import Markdown
from google.api_core.exceptions import InternalServerError  # Import the InternalServerError exception
import os,io 
import cv2 
import numpy as np 
from scipy.ndimage import interpolation as inter
import matplotlib.pyplot as plt 
from google.cloud import vision_v1 as vision
import fitz  # PyMuPDF
from PIL import Image


def to_markdown(text):
    text = text.replace('•', '  *')
    return Markdown(textwrap.indent(text, '> ', predicate=lambda _: True))

#extract info by gemini from img
def fix_output_img_gemini(qes, path, GOOGLE_API_KEY):
    #GOOGLE_API_KEY='AIzaSyBgNxAsN6uypF78G_Z0ScE9tNzf-dlmy3M'
    generation_config: str ={
      'temperature': 0.9,
      'top_p': 1,
      'top_k': 40,
      'max_output_tokens': 2048,
      'stop_sequences': [],
      }

    safety_settings : list[str] = [{"category": "HARM_CATEGORY_HARASSMENT", "threshold": "BLOCK_NONE"},
                    {"category": "HARM_CATEGORY_HATE_SPEECH", "threshold": "BLOCK_NONE"},
                    {"category": "HARM_CATEGORY_SEXUALLY_EXPLICIT", "threshold": "BLOCK_NONE"},
                    {"category": "HARM_CATEGORY_DANGEROUS_CONTENT", "threshold": "BLOCK_NONE"}]
    genai.configure(api_key=GOOGLE_API_KEY)
    # Check if img_or_path is a numpy array or a file path
    if isinstance(path, np.ndarray):
        # If it's a numpy array, convert it to PIL image
        img = PIL.Image.fromarray(path)
    else:
        # Otherwise, assume it's a file path
        img = path
    #img = PIL.Image.open(path)
    #img=path
    model = genai.GenerativeModel('gemini-pro-vision',generation_config=generation_config,safety_settings=safety_settings)
    response = model.generate_content(img)
    Markdown(response.text)
    response = model.generate_content([f'ما هو {qes} بالصورة :', img])
    ans = response.text
    return ans

#extract info by gemini from text
def fix_output_txt_gemini(message, GOOGLE_API_KEY):
    #GOOGLE_API_KEY='AIzaSyBgNxAsN6uypF78G_Z0ScE9tNzf-dlmy3M'
    genai.configure(api_key=GOOGLE_API_KEY)
    generation_config: str ={
      'temperature': 0.9,
      'top_p': 1,
      'top_k': 40,
      'max_output_tokens': 2048,
      'stop_sequences': [],
      }

    safety_settings : list[str] = [{"category": "HARM_CATEGORY_HARASSMENT", "threshold": "BLOCK_NONE"},
                    {"category": "HARM_CATEGORY_HATE_SPEECH", "threshold": "BLOCK_NONE"},
                    {"category": "HARM_CATEGORY_SEXUALLY_EXPLICIT", "threshold": "BLOCK_NONE"},
                    {"category": "HARM_CATEGORY_DANGEROUS_CONTENT", "threshold": "BLOCK_NONE"}]
    model = genai.GenerativeModel('gemini-pro',generation_config=generation_config,safety_settings=safety_settings)
    chat = model.start_chat(history=[])
    response = chat.send_message(message)
    return response.text
