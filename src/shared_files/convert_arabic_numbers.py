# this function to convert any arabic number to english and let english the same without any conversion
def arabic_to_english_numbers(num):
    arabic_numbers = '٠١٢٣٤٥٦٧٨٩'
    english_numbers = '0123456789'
    translation_table = str.maketrans(arabic_numbers, english_numbers)
    english_text = num.translate(translation_table)
    return english_text

def arabic_to_english(string):
    # Dictionary mapping Arabic numerals to English numerals
    arabic_to_english = {
        '۰': '0', '۱': '1', '۲': '2', '۳': '3', '۴': '4',
        '۵': '5', '۶': '6', '۷': '7', '۸': '8', '۹': '9'
    }
    # Iterate through each character in the string
    converted_string = ''
    for char in string:
        if char in arabic_to_english:
            # If the character is an Arabic numeral, convert it to English
            converted_string += arabic_to_english[char]
        else:
            # If the character is not an Arabic numeral, keep it as it is
            converted_string += char
    return converted_string

def convert_if_arabic(nums):
    if isinstance(nums, str) and any(char in '٠١٢٣٤٥٦٧٨٩' or char in '۰۱۲۳۴۵۶۷۸۹' for char in nums):
        nums = arabic_to_english_numbers(nums)
        nums = arabic_to_english(nums)
    return nums