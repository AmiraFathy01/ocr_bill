from flask import Flask, request, jsonify
from flask_cors import CORS
import os
import pandas as pd
from io import BytesIO
import numpy as np
import fitz
from PIL import Image
import cv2
import tempfile
from InvoiceData.process_img_text import invoice_info
from table.ProcessTable.extract_last_table import process_excel_file
from text_detector import TextDetector
import argparse
import json
import sys
import traceback

class ocrRun:
    def __init__(self, service_google, gemini_api, image_path, word_header_eng_path, word_header_ara_path, word_header_merge_path
                 , my_vat, percentage):
        self.service_google = service_google
        self.image_path = image_path
        self.word_header_eng_path = word_header_eng_path
        self.word_header_ara_path = word_header_ara_path
        self.word_header_merge_path = word_header_merge_path
        self.my_vat = my_vat
        self.percentage = percentage
        self.gemini_api = gemini_api
        
    def read_lines_from_file(self, file_path):
        with open(file_path, 'r', encoding='utf-8') as file:
            lines = [line.strip() for line in file.readlines()]
        return lines
    
    def pdf_to_images(self, pdf_path):
        doc = fitz.open(pdf_path)
        images = []
        for page_num in range(len(doc)):
            page = doc.load_page(page_num)
            text = page.get_text()
            if not text.strip():
                continue
            pix = page.get_pixmap()
            img = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
            images.append(np.array(img))
        return images
    
    def ext_text(self, image):
        word_header_eng = self.read_lines_from_file(self.word_header_eng_path)
        word_header_ara = self.read_lines_from_file(self.word_header_ara_path)
        word_header_merge = self.read_lines_from_file(self.word_header_merge_path)
        ocr = TextDetector(self.service_google)
        text, df = ocr.get_text_df(image, word_header_eng, word_header_ara, word_header_merge)
        if df is None or df.empty: 
            df = pd.DataFrame()
            excel_file = BytesIO()
            df.to_excel(excel_file, index=False)
            df = excel_file
        else:
            excel_file = BytesIO()
            df.to_excel(excel_file, index=False)
            excel_file.seek(0) 
            df = excel_file
        return text, df
    
    def bill_info(self, text, image):
        total = None
        sub = None
        inv_info = None
        text_processor = invoice_info()
        inv_info = text_processor.extract_text_from_pdf_or_image(text, self.my_vat, self.percentage, image, self.gemini_api)
        if inv_info is not None:
            if inv_info[10] is not None:
                total = inv_info[10]
            else:
                total = None
            if inv_info[8] is not None:
                sub = inv_info[8]
            else:
                sub = None
        return inv_info, total, sub
    
    def table_info(self, df, text, total, sub):
        processed_df, output_dict = process_excel_file(df, text, total, sub, self.gemini_api)
        return output_dict
    
    def process_one_image(self, image):
        total = None
        sub = None
        inv_info = None
        text, df = self.ext_text(image)
        inv_info, total, sub = self.bill_info(text, image)
        output_dict = self.table_info(df, text, total, sub)
        return text, df, inv_info, output_dict
    
    def call_process(self):
        total = None
        sub = None
        text_merge = ""
        inv_info = None
        output_dict_merge = []
        df_merge = pd.DataFrame()
        try:
            if self.image_path.endswith('.pdf'):
                images = self.pdf_to_images(self.image_path)
            else:
                images = [cv2.imread(self.image_path)]
            
            if len(images) == 1:
                text, df, inv_info, output_dict = self.process_one_image(images[0])
                return text, df, inv_info, output_dict
            else:
                for i, image in enumerate(images):
                    if i == len(images) - 1:
                        text, df = self.ext_text(image)
                        inv_info, total, sub = self.bill_info(text_merge, image)
                        output_dict = self.table_info(df, text, total, sub)
                    else:
                        text, df = self.ext_text(image)
                        output_dict = self.table_info(df, text, total, sub)
                    
                    text_merge += text
                    output_dict_merge.append(output_dict)
                    if type(df) is BytesIO:
                        df = pd.read_excel(df)
                    else:
                        df = pd.DataFrame()
                    df_merge = pd.concat([df_merge, df], ignore_index=True)
                        
                return text_merge, df_merge, inv_info, output_dict_merge
        except Exception as e:
            print(f"Error occurred: {str(e)}", file=sys.stderr)
            traceback.print_exc()
            return "", pd.DataFrame(), None, []

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("--gemini_api")
parser.add_argument("--service_google_path")
parser.add_argument("--word_header_eng_path")
parser.add_argument("--word_header_ara_path")
parser.add_argument("--word_header_merge_path")
parser.add_argument("--my_vat")
parser.add_argument("--percentage")
parser.add_argument("--file_path")
args = parser.parse_args()

# Assign values
Gemini_api = args.gemini_api
serviceGooglePath = args.service_google_path
wordHeaderEngPath = args.word_header_eng_path
wordHeaderAraPath = args.word_header_ara_path
wordHeaderMergePath = args.word_header_merge_path
myVat = args.my_vat
percentage = args.percentage
file_path = args.file_path

app = Flask(__name__)
CORS(app)

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']

    if file.filename == '':
        return jsonify({'error': 'No selected file'})

    try:
        temp_dir = tempfile.gettempdir()
        file_path = os.path.join(temp_dir, file.filename)
        file.save(file_path)
        
        return jsonify({'message': 'File uploaded successfully', 'file_path': file_path})
    except Exception as e:
        return jsonify({'error': str(e)})

@app.route('/process', methods=['POST'])
def process_file():
    data = request.json
    file_path = data.get('file_path')
    if not file_path:
        return jsonify({'error': 'No file path provided'})

    try:
        ocr = ocrRun(serviceGooglePath, Gemini_api, file_path, wordHeaderEngPath, wordHeaderAraPath, wordHeaderMergePath, myVat, percentage)
        text, df, inv_info, output_dict = ocr.call_process()
        
        if inv_info is not None and output_dict is not None:
            return jsonify({'inv_info': inv_info, 'output_dict': output_dict})
        else:
            return jsonify({'error': 'Processing failed. Check server logs for details.'})
    except Exception as e:
        print(f"Error occurred: {str(e)}", file=sys.stderr)
        traceback.print_exc()
        return jsonify({'error': str(e)})

if __name__ == '__main__':
    app.run(debug=False)
