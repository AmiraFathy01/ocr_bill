import re
import regex as re
#import numpy as np 
#import nltk
#nltk.download('punkt')
#from nltk import pos_tag, ne_chunk
#from nltk.tokenize import word_tokenize, sent_tokenize
#from nltk.tag import pos_tag
#from nltk.chunk import ne_chunk
#import stanza
#from nltk.tokenize import word_tokenize
#from nltk.metrics import jaccard_distance
#import pandas as pd
from InvoiceData.vat_num import extract_vat

# the function extract companies and vat 
def extract_company_vat(text, my_dict,my_vat):
    ignore_words = {"للتجارة","شركة", "مؤسسة", "التجارية", "company","Al","co","Co.","وكالة","agency","est","Est"}
    text = text.lower()  # Convert the text to lowercase for case-insensitive comparison
    # Initialize variables for company, vat, and line number
    company = None
    vat = None
    vat1 = None
    vat2= None
    line_number_vat_dic = None
    line_number_my_vat = None
    # Split the text into lines and iterate over them
    for line_num, line in enumerate(text.split('\n'), start=1):
        # Check if any key is present in the line will make key is vat and value in front of it is company
        for key in my_dict.keys():
            if key.lower().strip() in line.lower().strip():
                company = my_dict[key]
                vat = key
                line_number_vat_dic = line_num
                break  # Exit loop once a match is found
        # If no match is found in keys, check in values then if found make key is vat and value in front of it is company
        # and make search by values only in first 6 lines and search by company Arabic and English names for company 
        if company is None and line_num <= 6:
            for key, values in my_dict.items():
                tokens1 = set(word_tokenize(line.lower()))
                value_ar, value_en = values
                tokens2 = set(word_tokenize(value_ar.lower()))
                tokens3 = set(word_tokenize(value_en.lower()))
                # Exclude words from the ignore set
                tokens1 = tokens1 - ignore_words
                tokens2 = tokens2 - ignore_words
                tokens3 = tokens3 - ignore_words
                # Calculate Jaccard similarity between the sets of tokens
                jaccard_similarity = 1 - jaccard_distance(tokens1, tokens2)
                jaccard_similarity2 = 1 - jaccard_distance(tokens1, tokens3)
                # Count the number of common words
                common_words_count = len(tokens1.intersection(tokens2))
                common_words = len(tokens1.intersection(tokens3))
                #  فى بعض الاحيان ممكن اسم الشركة مكتوب و لكن بالمشقلب او بترتيب مختلف او مش مكتوبة كاملة فبيطلع مدى القرب اذا فى كلمتين على الاقل موجودين حتى لو مش بنفس الترتيب من اسم الشركة
                # Check if the similarity or common words count is above the threshold
                if jaccard_similarity >= 0.8 or common_words_count >= 2:
                    line_number_vat_dic = line_num
                    vat = key
                    company = values
                    break  # Stop searching after the first match
                elif jaccard_similarity2 >= 0.8 or common_words >= 2:
                    line_number_vat_dic = line_num
                    vat = key
                    company = values
                    break  # Stop searching after the first match
        if my_vat.lower().strip() in line.lower().strip():
            line_number_my_vat = line_num
            break  # Exit loop once my_vat is found
    if line_number_vat_dic is not None :
        #print(line_number_vat_dic)
        #print(vat)
        #print(line_number_my_vat)
        #print(my_vat)
        if line_number_my_vat  is not None: 
            if line_number_vat_dic <line_number_my_vat :
                vat1=vat
                vat2=my_vat
            else :
                vat1=my_vat
                vat2=vat
        else :
            vat1= vat
    # there is no company and vat in text in the dictionary (database) so it will go to function extract_vat to search and by this vat try again 
    #to see if company in the databese ( because numbers can be have spaces for that it is not extract it in that part of code)
    if company is None and vat is None:
        vat1, vat2= extract_vat(text)
        vat1= vat1 if vat1 else None
        vat2 = vat2 if vat2 else None
        if vat1 is not None:
            for key in my_dict.keys():
                if key.lower().strip() in vat1.lower().strip():
                    company = my_dict[key]
                    break  # Exit loop once a match is found
                company = None
        return company,vat1,vat2,vat

    return company,vat1,vat2,vat
    
# this function depend on function extract_company_vat and vat1 and vat2 say the type of the invoice and return it ( شراء أو بيع ) 
def extract_type_of_invoice(text, my_dict,my_vat):
    type_invoice= None
    company,vat1,vat2,vat=extract_company_vat(text, my_dict,my_vat)
    if vat1 is not None:
        if vat1 == my_vat:
            type_invoice="sales"
            return type_invoice
        elif vat2 is not None:
            if vat2==my_vat:
                type_invoice="purchases"
                return type_invoice
        elif vat1 != my_vat:
            type_invoice="purchases"
            return type_invoice
    return None
    
# فى كود الشركات لو كذا شركة بيطلع كله سوا بالعربى و الانجليزى و مش بيطلع بس اللى فى النص و لكن هنا على حسب الشركات اللى فى ال tuple
# بيقول انهى واحدة فيهم حسب الكلام اللى بالنص بيدور تانى و يقول انهى واحد فيهم يخرج الشركة و ال tuple الخاص بها
def search_tuples_in_paragraph(text, tuples):
    text = text.lower()
    matches = []
    for tpl in tuples:
        found = False
        for sent in tpl:
            if sent and sent.lower() in text:
                found = True
                break
        if found:
            matches.append(tpl)
    return matches

# Function to replace 'nan' with ' ' in the tuple elements
def replace_wrong(tuple_data):
    new_tuple = tuple(data.replace('nan', ' ') for data in tuple_data)
    return new_tuple