# Importing the Libraries
#from spellchecker import SpellChecker
#from PIL import Image
#import PIL
#import pytesseract
import re
#import cv2
#import fitz  # Import from PyMuPDF
import langid
from langid.langid import LanguageIdentifier, model
import regex as re
#import numpy as np 
#import nltk
#nltk.download('punkt')
#from nltk import pos_tag, ne_chunk
#from nltk.tokenize import word_tokenize, sent_tokenize
#from nltk.tag import pos_tag
#from nltk.chunk import ne_chunk
#import stanza
import requests
#from nltk.tokenize import word_tokenize
#from nltk.metrics import jaccard_distance
#import pandas as pd
#import pathlib
#import textwrap
#import google.generativeai as genai
#from IPython.display import display
#from IPython.display import Markdown
from google.api_core.exceptions import InternalServerError  # Import the InternalServerError exception
from InvoiceData.norm_inv_info import it_only_inv
from InvoiceData.tax_inv_info import it_tax_inv
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.reverse_text import reverse_arabic_words_inv

########################################################
# this class for function we need to extract from invoice informations that we need
class invoice_info:
    def __init__(self):
        # helping in identify the language if there in text more than one language without error
        self.identifier = LanguageIdentifier.from_modelstring(model, norm_probs=True)   
    
    def some_method(self):
        # You can use self.my_dict and self.vat in other methods of the class
        pass


    def pdf_to_images(self,pdf_path):
        doc = fitz.open(pdf_path)
        images = []
        for page_num in range(len(doc)):
            page = doc.load_page(page_num)
            text = page.get_text()
            if not text.strip():  # Check if the page has no text content
                continue  # Skip empty pages
            pix = page.get_pixmap()
            img = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
            images.append(np.array(img))
        return images

    
# the function that in it all previous function execute by calling it and with the path for the file
# this function first remove from text any of this symbols : [\[\]\{\}\<\>\(\)\|~:;?!] then reverse arabic text to be by the right order before work
#then it check if it is invoice or not , if invoice and found the words that say it is invoice then only excute the functions else it is not invoice
    def extract_text_from_pdf_or_image(self, text, my_vat, percentage, path,GOOGLE_API_KEY):
        text = str(text)
        ttext= text
        pattern = r'[\[\]\{\}\<\>\(\)\|~:;?!#]'
        text = re.sub(pattern, '', text)
        inv_output= None
        try:
            #split_tuple= None
            text = reverse_arabic_words_inv(text)
            #print(text)
            tax_invoice_words = ['simplified invoice','sales tax invoice','simplified sales tax invoice','simplified tax invoice','simplified tax','sales taxation invoice','taxation invoice','tax invoice',"vat invoice",'ضريبية فـاتـوره','فاتوره ضريبية','فاتورة مبيعات ضريبية','الضريبية فاتوره','الضريبية فاتورة','ضريبية مبيعات فاتورة',"  مبسطة ضريبية جانوره", 'فاتوره ضريبية مبسطة',"ضريبيه مبسطة",'ضربة فاتورة','فاتورة ضربة','ضريبية فاتوره','ضريبية فـاتـورة','فاتورة ضريبية',"  مبسطة ضريبية جانورة", 'فاتورة ضريبية مبسطة',"ضريبية مبسطة",'ضريبية فاتورة', 'tax invoice','invoice tax','مبسطة ضريبية فاتوره','مبسطة ضريبية فاتورة']
            if any(word in text for word in tax_invoice_words):
                #print("It is tax invoice")
                inv_output= it_tax_inv(text,ttext, my_vat, percentage, path,GOOGLE_API_KEY)
                ####
            else:
                # Display a message indicating it's not a valid invoice
                #print("It is not a normal invoice, not tax invoice")
                inv_output= it_tax_inv(text,ttext, my_vat, percentage, path, GOOGLE_API_KEY)
                #it_only_inv(text,ttext, my_dict, my_vat, percentage, path, GOOGLE_API_KEY)
                
        except InternalServerError as e:
            print(f"An internal server error occurred: {e}")
            # Optionally, you can log the error or take other actions
        except Exception as ex:
            print(f"An unexpected error occurred: {ex}")
            
        return inv_output