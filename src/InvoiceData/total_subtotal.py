import re
import regex as re
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.numbers_modify import convert_amount_format_inv
# this function extract the total and subtotal from text
def extract_total_subtotal(text):
    text = text.lower()
    total =None
    subtotal=None
    subtotal2 = None
    # this phases to match the total 
    search_words =["الإجمالي شامل الضريبة","الإجمائى","الإجماليى","الاجمائى","الاجماليى"     ,
                   "total","tota","subtotal","sub total","يشمل قيمة الضريبة المضافة", "يشمل ضريبة القيمة المضافة",
        "الصافي\+الضريبة", "الصافى \+ الضريبة", "الصافى \+ الضريبة","الصافى", "صافى", "الرصيد المستحق", " المستحق",
        "قيمة الفاتورة", "الاجمالى", "الإجمالى", "الإجمالي", "الاجمالي", 'الإجمالي ',    "المجموع",'الصافي','صافي', "مجموع", "إجمالى", "اجمالي", "إجمالي", "اجمالى "
        ,"الإجسالي ","الصافي","الإجمالي شامل الضريبة","المبلغ"    ,'المستحق' ,'القيمة المضافة','amount','net price','net'  ]
    # phrase to don't matchs
    exclude_phrase = ["total tax","اجمالي الضريبة","tax total","الضريبة اجمالي",'total vat','إجمالي ضريبة','قيمة إجمالي الضريبة','إجمالي الضريبة']
    # pattern exclude
    pattern_exclude = re.compile(fr'\b(?:{"|".join(map(re.escape, exclude_phrase))})\b', flags=re.IGNORECASE)
    #pattern to match 
    pattern = re.compile(fr'\b(?:{"|".join(map(re.escape, search_words))})\b', flags=re.IGNORECASE)
    ##########
    # phrase to don't matchs
    exclude_phrase2 = ['sub','sub ', 'subtotal', 'قبل الضريبة','without tax','before tax','غير شامل ضريبة القيمة المضافة','غير شامل الضريبة','غير شامل ضريبة']
    # pattern exclude
    pattern_exclude2 = re.compile(fr'\b(?:{"|".join(map(re.escape, exclude_phrase2))})\b', flags=re.IGNORECASE)
    # pattern to match number 
    number_pattern = r'(?P<number>\d+,?\d*\.?\d*\.?\d*)'
    #number_pattern = r'(?P<number>\d+,?\d*\.?\d*\.?\d*)\s*(?![\d]*%\b)'

    #number_pattern = r'(?P<number>\d+(?:[.,]\d+)?)'

    #number_pattern=r'\d*'                                          
    totals=[]
    # search for total and subtotal line by line in text and with consider not be from exclude pattern that have pattern for words to do not match
    for line in text.split('\n'):
        matches = re.search(pattern , line)
        if matches:
            #print("hh ",matches)
            exclude=re.search(pattern_exclude , line)
            #print(f"exclude: {exclude}")
            if not exclude :
                #print(f"{exclude}")
                matched_word = matches.group(0)
                #print(line)
                #match_number= re.search(number_pattern,line,re.IGNORECASE)
                match_number= re.findall(number_pattern,line,re.IGNORECASE)
                numbers_line=[]
                for match in match_number :
                    if match:
                        # if number float
                        if "."in match or ',' in match :
                            #print(match)
                            number =match  # Group 2 captures the number
                            #print(f"Number: {number}")
                            #numeric_value = float(number)  # Convert the numeric value to float
                            total_to_convert= number  # make the text currency and number 
                            #print(total_to_convert)
                            convert_total= convert_amount_format_inv(total_to_convert) # call the function to convert the currency 
                            #print(f" the convert total d{convert_total}")
                            numbers_line.append(float(convert_total))                                                                      
                        else : 
                            if match == '15' and '%' in line:
                                continue
                            else:
                                number = match
                                numbers_line.append(int(number))
                        #print("***************************")
                        #print("this is  exclude word")
                ###
                if numbers_line:
                    #print("nnnnn ",numbers_line)
                    totals.append(max(numbers_line))
                    #print("fffff ",totals)
                    if re.search(pattern_exclude2, line):
                        # If pattern_exclude2 is found, put the number in subtotal
                        subtotal2 = min(numbers_line)
                        subtotal2 = round(subtotal2, 2)
        #else:
            #print("No matching word found.")
    if totals :
        #print("ddd ",totals)
        totals = [num for num in totals if num <= 10000000]
        #print("Original totals:", totals)
        if len(totals) > 2:
            # Count the occurrences of integers and floats in totals
            int_count = sum(isinstance(num, int) for num in totals)
            float_count = sum(isinstance(num, float) for num in totals)
            #print("Integer count:", int_count)
            #print("Float count:", float_count)
            # Check if both integers and floats are present and there are more integers
            if int_count >= 0 and float_count > 0 and int_count < float_count:
                # Filter out integers from the totals list
                totals = [num for num in totals if isinstance(num, float)]
                #print("Filtered totals:", totals)
            elif int_count > 0 and float_count >= 0 and int_count > float_count:
                # Filter out integers from the totals list
                totals = [num for num in totals if isinstance(num, int)]
                totals = [float(num) for num in totals]
                #print("Filtered totals:", totals)   
        elif len(totals) == 2:
            # Count the occurrences of integers and floats in totals
            int_count = sum(isinstance(num, int) for num in totals)
            float_count = sum(isinstance(num, float) for num in totals)
            #print("Integer count:", int_count)
            #print("Float count:", float_count)
            if int_count == 1 and float_count == 1 and int_count == float_count:
                # Filter out integers from the totals list
                totals = [num for num in totals if isinstance(num, float)]
                #print("Filtered totals:", totals)
            elif int_count == 2 and float_count == 0:
                # Filter out integers from the totals list
                totals = [num for num in totals if isinstance(num, int)]
                totals = [float(num) for num in totals]
                #print("Filtered totals:", totals)                      
            elif int_count == 0 and float_count == 2:
                # Filter out integers from the totals list
                totals = [num for num in totals if isinstance(num, float)]
                #print("Filtered totals:", totals)
        if totals:
            # if there is numbers in totals list and min and max not the same list take the max as total and min as subtotal and (2 numbers just after .)
            if min(totals)!=max(totals):
                total=max(totals)
                total=round(total,2)
                subtotal= min(totals)
                subtotal=round(subtotal,2)
            # if there only one number in totals list to take it as total and (2 numbers just after .) and there is no subtotal
            elif min(totals)==max(totals):
                total=max(totals)
                total=round(total,2)
                subtotal= None 
            if subtotal2 != None:
                if subtotal != subtotal2:
                    subtotal = subtotal2
                    if total == subtotal:
                        total = None
        else:
            total= None
            subtotal= None
    #print(f"largest = {largest} \n smallest= {smallest}")
    return total, subtotal