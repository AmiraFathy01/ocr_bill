import re
import regex as re
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.numbers_modify import convert_amount_format_inv
from shared_files.convert_arabic_numbers import arabic_to_english_numbers, arabic_to_english

def inv_num(text, exclude_numbers=None):
    #text = re.sub(r'[.:]', '', text)
    invoice_num = []
    words = ['inv no','inv num','invoice num','invoice no','inv number', 'invoice number','voucher no','invoice #','رقم المستند'
             ,'فاتورة رقم','رقم السند الفاتورة','الرقم الفاتورة','voucher num','voucher number'
             ,'رقم الفتورة','قم الفاتوره','قم المستند','فاتورة قم','قم الفاتورة','رقم الفاتوره'
             ,'قم الفاتورة','رقم فاتورة','رقم الفاتورة','الفاتورة رقم','رقم الفاتوره', 'invoice رقم','رقم invoice']
    exclude_float_num = []
    if exclude_numbers is not None:
        for exclude2 in exclude_numbers:
            # if there numbers in exclude
            # and if the numbers of exclude int or float exclude it and add it in number_float
            if exclude2 is not None:
                if isinstance(exclude2, (int, float)):
                    number_float = float(exclude2)
                    exclude_float_num.append(number_float)
                # this part of the code checks if exclude contains digits, and if so, it attempts to extract a numerical value from it,
                #converts it to a float, and appends it to the exclude_float_num list.
                elif any(char.isdigit() for char in exclude2):
                    exclude2 = convert_amount_format_inv(exclude2)
                    number_float = float(exclude2)
                    exclude_float_num.append(number_float)
                        ############################
    pattern = re.compile(fr'\b(?:{"|".join(map(re.escape, words))})\b', flags=re.IGNORECASE)
    # phrase to don't matchs
    exclude_phrase = ['vat','vin','plate','الطلب','البائع','صفحة','الصفحة','toll free','trans','tin','المجانى','المجاني','id','lpo','التعميد','لأمرر','الضريبه' ,'customer','cust', 'client','supplier','العميل','total','ref','house','البيت','المنزل','reference','مرجع','المرجع','جوال','جول','تيليفون','التليفون','هاتف','الهاتف','تليفون','tele','tel','tax','الضريبة','الضريبي','الضريبى','ضريبي','total','account','الحساب','mobile','الاضافي','building','المبني','السجل','المبني','cr','mob','additional','phone','الحساب','الموبيل','الصنف','الوصف','item','warehouse']
    # pattern exclude
    pattern_exclude = re.compile(fr'\b(?:{"|".join(map(re.escape, exclude_phrase))})\b', flags=re.IGNORECASE |re.UNICODE)
    # additional pattern for invoice numbers
    #number_pattern = r'\b(?:[A-Za-z]{1}\d{2}-\d{7}|\d{5,})\b'
    #number_pattern = r'\b(?:[A-Za-z]{1}\d{2}-\d{7}|\d{5,})\b|\d+'
    number_pattern = r'\b(?:[A-Za-z]{1}\d{2,}-\d{7,}|[A-Za-z]{1}\d{2,}|\d{3,})\b' 
    text_lines = text.split('\n')[:12]
    for line in text_lines:
        # Find all matches of the pattern in the text line
        matches = re.findall(pattern, line)
        for match in matches:
            # Check if any exclude phrases appear before the match
            if re.search(fr"\b(?:{'|'.join(map(re.escape, exclude_phrase))})\b", line.split(match)[0]):
                continue  # Skip this match if any exclude phrase appears before it
            # If the pattern matched and no exclude phrase appears before it, find invoice number in the vicinity
            match_number = re.findall(number_pattern, line)
            for num in match_number:
                invoice_num.append(num)
                #break  # Once found, break out of the loop
    f_pattern =  r'(\d+)\s*[:/-]+\s*(\d+|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec|january|february|march|april|may|june|july|august|september|october|november|decemberيناير|فبراير|مارس|أبريل|ابرايل|ابريل|إبرايل|مايو|يونيو|يوليو|أغسطس|اغسطس|إغسطس|سبتمبر|أكتوبر|اكتوبر|إكتوبر|نوفمبر|ديسمبر)\s*[:/-]?\s*(\d+)?'
    # Find all matches in the text
    f_matches = re.findall(f_pattern, text)
    #print(f_matches)
    extracted_numbers = []
    # Iterate through matches and extract numbers
    for match in f_matches:
        extracted_numbers.extend(match)
    # Exclude specified numbers
    if exclude_numbers is not None:
        invoice_num = [num for num in invoice_num if num not in exclude_numbers and num not in extracted_numbers and not num.endswith('003')]
    if invoice_num:
        #print("yes")
        invoice_num = invoice_num[0]
    else:
        invoice_num = None

    if invoice_num is not None:
        #print("yes")
        if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in invoice_num) and invoice_num:
            invoice_num = arabic_to_english_numbers(invoice_num)
            invoice_num = arabic_to_english(invoice_num)
    return invoice_num