import re
import regex as re
import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.numbers_modify import convert_amount_format_inv

# this function extract discount percentage or amount from text
def extract_discount(text):
    text = text.lower()
    # Words to search for before or after the numbers
    search_words = ['discount', 'الخصم', 'خصم', 'خصومات', 'save', 'Limited-time offer']
    # Constructing the pattern
    pattern = re.compile(fr'\b(?:{"|".join(map(re.escape, search_words))})\b', flags=re.IGNORECASE)
    # pattern to match number 
    number_pattern = r'(?P<number>\d+,?\d*\.\d+\.?\d*)'
    extracted_data = []
    for line in text.split('\n'):            
        matches = re.search(pattern , line)
        if matches:
            matched_word = matches.group(0)
            match_number= re.findall(number_pattern,line,re.IGNORECASE)
            #numbers_line=[]
            for match in match_number :
                if match:
                    if "."in match or ',' in match :
                        number =match  # Group 2 captures the number
                        dis_to_convert= number  # make the text currency and number 
                        convert_dis=convert_amount_format_inv(dis_to_convert) # call the function to convert the currency 
                        extracted_data.append(convert_dis)
                #if numbers_line:
                    #extracted_data.append(numbers_line)
        # If the first match is found, skip the loop
        if extracted_data:
            break
    # if there is no discount in text so it will be 0.0
    if not extracted_data:
        extracted_data = '0.0'

    return extracted_data