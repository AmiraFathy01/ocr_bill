# this function for calculate the total and subtotal and tax and discount and all accounting information
def extract_accounting_results(total, subtotal, pre, taxes, discount):
    discount_percentage = 0.0
    discount_amount = 0.0
    total_before_discount = 0.0
    total_after_discount = 0.0

    # Extract relevant numbers and information
    # convert discount to string and take first number in the list 
    C_dis = str(discount[0])
    C_dis = ''.join(c for c in C_dis if c.isdigit() or c == '.')
    # to split from percentage the % and convert it to integer
    perc = pre.strip("%")
    percentage = int(perc)
    # if from the previous functions there is the percentage and total numbers extracted so we will calc the accounting informations depending on them
    if percentage is not None and total is not None:
        # Calculate Subtotal Before Tax (SBT)
        calc_subtotal = round(total / (1 + (percentage / 100)), 2)
        # Calculate Tax Amount (TA)
        calc_tax = round(total - calc_subtotal, 2)
        # Calculate Total After Tax (TAT)
        calc_total = total
        # Calculate Tax Percentage
        calc_tax_percentage = percentage
        # if discount is not None to start do the other calculations
        if C_dis != '0.0':
            # the discount is percentage , so split from it % and convert it to int
            if '%' in C_dis:
                # If '%' is present, treat dis as discount percentage
                discount_percentage = int(C_dis.strip("%"))
                discount_amount = round(calc_subtotal * (discount_percentage / 100), 2)
            else:
                # If no '%' is present, treat dis as discount amount
                discount_amount = float(C_dis)
                discount_percentage = round((discount_amount / calc_subtotal) * 100 ,2)
            # Calculate Total Before Discount (TBD)
            total_before_discount = round(calc_subtotal + discount_amount, 2)
            # Calculate Total After Discount (TAD)
            total_after_discount = calc_subtotal
        else:
            #print("C_dis is '0.0'")
            discount_percentage = 0.0  # Use numerical value instead of string literal
            discount_amount = 0.0  # Use numerical value instead of string literal
            total_before_discount = calc_total # Use numerical value instead of string literal
            total_after_discount = calc_total # Use numerical value instead of string literal

    #if from previous functions there is percentage number and tax extracted so the calculation will be depend on it , and total is none

    elif percentage is not None and taxes is not None:
        # Assuming taxes is a list, use the first element
        #taxes = taxes[0]
        # Calculate the total using the formula: Total = Subtotal + Tax Amount
        calc_total = round(taxes / (percentage / 100), 2)
        # Calculate the subtotal using the formula: Subtotal = Total / (1 + Tax Percentage / 100)
        calc_subtotal = round(calc_total / (1 + (percentage / 100)), 2)
        calc_tax = taxes
        calc_tax_percentage = percentage
        if C_dis != '0.0':
            if '%' in C_dis:
                # If '%' is present, treat dis as discount percentage
                discount_percentage = int(C_dis.strip("%"))
                discount_amount = round(calc_subtotal * (discount_percentage / 100), 2)
            else:
                # If no '%' is present, treat dis as discount amount
                discount_amount = float(C_dis)
                discount_percentage = round((discount_amount / calc_subtotal) * 100 ,2)

            # Calculate Total Before Discount (TBD)
            total_before_discount = round(calc_subtotal + discount_amount, 2)
            # Calculate Total After Discount (TAD)
            total_after_discount = calc_subtotal
        else:
            #print("C_dis is '0.0'")
            discount_percentage = 0.0  # Use numerical value instead of string literal
            discount_amount = 0.0  # Use numerical value instead of string literal
            total_before_discount = calc_total # Use numerical value instead of string literal
            total_after_discount = calc_total # Use numerical value instead of string literal    

    #if from previous functions there is percentage number and subtotal extracted so the calculation will be depend on it , and total and tax is none
    elif percentage is not None and subtotal is not None:
        # Calculate Total After Tax (TAT)
        calc_total = round(subtotal + (subtotal * (percentage / 100)), 2)
        # Calculate Tax Amount (TA)
        calc_tax = round(calc_total - subtotal, 2)
        calc_tax_percentage = percentage
        calc_subtotal = subtotal
        if C_dis != '0.0':
        #####
            #print("C_dis is not '0.0'")
            if '%' in C_dis:
                #print("Percentage discount")
                dis_str = C_dis.strip("%")
                # If '%' is present, treat dis as discount percentage
                discount_percentage= int(dis_str)
                discount_amount = round(calc_subtotal * (discount_percentage / 100), 2)
            
            else:
                #print("Amount discount")
                # If no '%' is present, treat dis as discount amount
                discount_amount = float(C_dis)
                discount_percentage = round((discount_amount / calc_subtotal) * 100 ,2)

            # Calculate Total Before Discount (TBD)
            total_before_discount = round(calc_subtotal + discount_amount, 2)
            # Calculate Total After Discount (TAD) 
            total_after_discount = calc_subtotal

        else:
            #print("C_dis is '0.0'")
            discount_percentage = 0.0  # Use numerical value instead of string literal
            discount_amount = 0.0  # Use numerical value instead of string literal
            total_before_discount = calc_total # Use numerical value instead of string literal
            total_after_discount = calc_total # Use numerical value instead of string literal
    else:
        discount_percentage = None
        discount_amount = None
        total_before_discount = None
        total_after_discount = None
        calc_total = None
        calc_tax = None
        calc_tax_percentage = percentage
        calc_subtotal = None
    #print('Total After Tax (TAT)', calc_total)
    #print('Subtotal Before Tax (SBT)', calc_subtotal)
    #print('Tax Amount (TA)',calc_tax)
    #print('Tax Percentage', calc_tax_percentage)
    #print('Discount Amount (DA)', discount_amount)
    #print('Discount Percentage', discount_percentage)
    #print('Total Before Discount (TBD)', total_before_discount)
    #print('Total After Discount (TAD)', total_after_discount)

    return calc_subtotal,calc_tax, calc_total ,calc_tax_percentage, discount_amount,discount_percentage, total_before_discount, total_after_discount