import re
import regex as re
# this function extract the percentage from text 
def extract_percentages(text):
    # Define the pattern to match percentages
    # pattern that extract any number followed by % or previoused 
    pattern = r'(?P<number_before>\d+(\.\d+)?)%\s*|\s*(?P<number_after>\d+(\.\d+)?)% |%(?P<number_before2>\d+(\.\d+)?)\s*|\s*%(?P<number_after2>\d+(\.\d+)?)'

    # Extract percentages using the pattern
    matches = re.finditer(pattern, text)
    # Extract numbers with '%' symbol
    percentages_with_symbol = None
    # loop for all matches pattern
    for match in matches:
        number_before = match.group('number_before') or match.group('number_before2')
        number_after = match.group('number_after') or match.group('number_after2')
        #if number before %
        if number_before is not None:
            percentages_with_symbol= float(number_before)
        #if number after %
        elif number_after is not None:
            percentages_with_symbol=float(number_after)
    
    return percentages_with_symbol