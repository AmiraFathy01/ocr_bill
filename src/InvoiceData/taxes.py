import re
import regex as re
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.numbers_modify import convert_amount_format_inv

# function to extract tax  
def extract_tax_numbers(text, exclude_numbers=None):
    # Define the tax-related phrases
    text = text.lower()
    # the phrases to extract the tax 
    tax_phrases = ["taxes", " tax amount","tax", "taxable amount", "fee", "vat", "tax", "ضريبة","ضريبه "
                   " الضريبة", "ضريبة المبيعات", " الضريبة", " vat amount",  "الضريبة المدفوعة",
                   "الضرائب", " inc. vat", " inc vat", "sales tax", "الضريبة" , "ضريبة القيمة المضافة",
                   " ضريبة القيمة المضافة","الضريبه" ,"ضريبه"]
    #precentage= None
    amounts_with_currency = []
    #exclude list exclude numbers in total and subtotal and vat 
    # this part of the code is responsible for extracting numeric values from the exclude_numbers list,
    #converting them to floats, and storing them in the exclude_float_num list.
    #It handles both integer/float values directly provided in the list and values extracted from strings containing digits.
    exclude_float_num = []
    if exclude_numbers is not None:
        for exclude in exclude_numbers:
            #print(f"the exclude is {exclude}")
            # if there numbers in exclude
            # and if the numbers of exclude int or float exclude it and add it in number_float
            if exclude is not None:
                if isinstance(exclude, (int, float)):
                    number_float = float(exclude)
                    exclude_float_num.append(number_float)
                # this part of the code checks if exclude contains digits, and if so, it attempts to extract a numerical value from it,
                #converts it to a float, and appends it to the exclude_float_num list.
                elif any(char.isdigit() for char in exclude):
                    exclude = convert_amount_format_inv(exclude)
                    number_float = float(exclude)
                    exclude_float_num.append(number_float)
                    
    # words that if after tax not take the number it is not tax
    exclude_phrase=["شامل الضريبة",'غير شامل ضريبة','غير شامل الضريبة',"شامل الضريبه","بدون الضريبه","بدون الضريبة","قبل الضريبة",'الرقم الضريبي','رقم الضريبي',"قبل الضريبه", "رقم الضريبة","Tax Invoice",
                    'num','id','رقم','الرقم','total without','total wihout','w/out','الإجمالي الضريبة']
    # Constructing the pattern
    pattern = re.compile(fr'\b(?:{"|".join(map(re.escape, tax_phrases))})\b', flags=re.IGNORECASE)
    pattern_exclude = re.compile(fr'\b(?:{"|".join(map(re.escape, exclude_phrase))})\b', flags=re.IGNORECASE)
    number_pattern = r'(?P<number>%?\d+,?\d*\.?\d*\.?\d*%?)'

    # Extracting information from matches_after
    for line in text.split('\n'):
        matches = re.search(pattern, line)
        if matches:
            exclude = re.search(pattern_exclude, line)
            # number is not from the exclude numbers and matches the pattern to extract tax number that match
            if not exclude:
                type_tax = "amount"
                matched_word = matches.group(0)
                match_number = re.findall(number_pattern, line, re.IGNORECASE)
                numbers_line = []
                for match in match_number:
                    # number is amount if not has after it % and float
                    if "%" not in match and ('.' in match or ',' in match):
                        # if the number of tax 1590 or 1596 not to take it
                        if match != 1590 and match != 1596 and match != "1590" and match != "1596":
                            number = match
                            #print(f"the number in tax is {number}")
                            tax = number
                            # after extract the tax numbers to convert it like in the format of numbers in extract_currency_and_amount function
                            convert_tax = convert_amount_format_inv(tax)
                            #print(type(convert_tax))
                            #print(f" number after convert {convert_tax}")
                            if convert_tax is not None:
                                if exclude_float_num is not None and float(convert_tax) in exclude_float_num:
                                    #print(f"This number is in exclude_numbers: {convert_tax}")
                                    continue
                                else:
                                    roun = round(convert_tax, 2)
                                    #print(f"round = {roun}")
                                    amounts_with_currency.append(convert_tax)
                                #print(f"tax {tax}")
                                #print(f"convert_tax {convert_tax}")
                            #else:
                                #precentage = "15%"
                                #print(f"precentages1 {precentage}")
                        # that mean it is not 15 and try to extract the percent of tax
                        #if "%" in match:
                            #precentage = match
                            #print(f"precentages2 {precentage}")
    #print(f"amounts_with_currency {amounts_with_currency}")
    if amounts_with_currency:
        # Extract the first element of the list and remove leading/trailing whitespaces
        ##first_vat = str(amounts_with_currency[0]).strip()
        #print(first_vat)
        ##amounts_with_currency=first_vat
        ##amounts_with_currency=float(amounts_with_currency)
        ##amounts_with_currency=round(amounts_with_currency,2)
        amounts_with_currency= round(float(amounts_with_currency[0]), 2)  # Return the first tax amount found
    else:
        amounts_with_currency = None
    #if amounts_with_currency ==0.0:
    #    precentage ="0%"
    #if precentage==None :
     #   precentage ="15%"
        #print(f"precentages3 {precentage }")
    return amounts_with_currency