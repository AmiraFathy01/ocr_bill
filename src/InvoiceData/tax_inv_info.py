from InvoiceData.bank_info import extract_bank_info
from InvoiceData.calculations import extract_accounting_results
from InvoiceData.vat_num import extract_vat
#from InvoiceData.companies import extract_company_vat, extract_type_of_invoice, search_tuples_in_paragraph, replace_wrong
from InvoiceData.dates import extract_dates
from InvoiceData.discount_num import extract_discount
from InvoiceData.inv_number import inv_num
from InvoiceData.inv_type import bank_or_nakd
from InvoiceData.taxes import extract_tax_numbers
from InvoiceData.total_subtotal import extract_total_subtotal
import re
import regex as re
from google.api_core.exceptions import InternalServerError  # Import the InternalServerError exception
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.extract_with_gemini import to_markdown, fix_output_img_gemini, fix_output_txt_gemini
from shared_files.convert_arabic_numbers import arabic_to_english_numbers, arabic_to_english

    
def it_tax_inv(text,ttext, my_vat, percentage, path,GOOGLE_API_KEY):
    #excluded_words = ['type', 'النوع','الدفع','payment type', 'pay type','cust','customer','العميل','اسم العميل','نوع الفاتورة','نوع','فاتورة','نوع السداد', 'نوع البيع','pay terms','فاتورة الى','فاتورة مبيعات','payment','طريقة الدفع','mode of sales']
    company_name = None
    inv_nums= None
    vat = None
    vat1= None
    date = None
    due_date = None
    date_american= None
    due_date_american= None
    type_NB= None
    comp_names= None
    modified_tuple= None
    ch= None
    excluded_words=[]
    vat_nums= None
    sub_total= None
    tax_nums= None
    phrase_list= ['رقم', 'فاتورة', 'الفاتورة','هو',':','.']
    total_nums= None
    type_com = None
    equations = [None, None, None]       
    total,subtotal = extract_total_subtotal(text)
    #print(f"total :{total} , subtotal : {subtotal}")
    #print("\n --------------------------------------------------------------------------------------------------------------\n")
    vat1,vat2=extract_vat(text)
    #print(" before vat1: ",vat1)
    #company_name, vat1,vat2,vat= self.extract_company_vat(text, my_dict,my_vat)
    #print("companies names: ",company_name)
    #print(" vat1: ",vat1)
    #print(" vat2:",vat2)
    #if company_name is None:
        #response1 = chat.send_message(f"ما هو اسم الشركة صاحبة الفاتورة   (company name): بهذا النص {text}")
        #comps=response1.text
        #if 'لم' in comps or 'لا' in comps or 'غير' in comps or 'no' in comps or 'not' in comps or 'ليس' in comps or 'ليست' in comps:
            #comps= None
            #comps= self.fix_output('اسم الشركة',path)
            #print("Companies that found: ",comps)
        #else:
            #comps = comps
            #print("Companies that found: ",comps)
    if vat1 is None:
        vats = fix_output_txt_gemini(f"ما هو الرقم الضريبي للعميل بتلك الفاتورة  (vat): بهذا النص {text}",GOOGLE_API_KEY)
        if 'لم' in vats or 'لا' in vats or 'غير' in vats or 'no' in vats or 'not' in vats or 'ليس' in vats or 'ليست' in vats:
            vats= None
            vats= fix_output_img_gemini(' الرقم الضريبي للعميل',path,GOOGLE_API_KEY)
            vats= re.sub(r'[^\d]', '', vats)
            if vats is not None:
                if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in vats) and vats:
                    vats = arabic_to_english_numbers(vats)
                    vats = arabic_to_english(vats)
            #print('Vats that found: ',vats)
        else:
            vats= re.sub(r'[^\d]', '', vats)
            if vats is not None:
                if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in vats) and vats:
                    vats = arabic_to_english_numbers(vats)
                    vats = arabic_to_english(vats)
            #print('Vats that found: ',vats)
    #if company_name is not None: 
        #split_tuple = tuple(sentence.split(', ') for sentence in company_name)
        #print(split_tuple)
        #result_list = []
        # Get the lengths of both lists
        #len_first_list = len(split_tuple[0])
        #len_second_list = len(split_tuple[1])
        # Iterate through the length of the shorter list
        #for i in range(min(len_first_list, len_second_list)):
            # Strip spaces from elements and pair them
            #pair = (split_tuple[0][i].strip(), split_tuple[1][i].strip())
            # Check if any word in the pair is "nan" and replace it with " "
            #pair = tuple(" " if word.lower() == "nan" else word for word in pair)
            #result_list.append(pair)
        #print("Companies by Arabic and English",result_list)
        #result = self.search_tuples_in_paragraph(text, result_list)
        #if result is not None:
            #for match in result:
                #modified_tuple = self.replace_wrong(match)
                #print("the final true company: ",modified_tuple)
        #ch = result_list
        #for index, tpl in enumerate(ch):
            #if 'nan' in tpl:
                #corrected_tpl = (tpl[0], ' ')
                #ch[index] = corrected_tpl
        #ch = ch
        #print("All Companies by Arabic and English: ",ch) 
    excluded_words = ['type', 'النوع','الدفع','payment type', 'pay type','cust','customer','العميل','اسم العميل','نوع الفاتورة','نوع','فاتورة','نوع السداد', 'نوع البيع','pay terms','فاتورة الى','فاتورة مبيعات','payment','طريقة الدفع','mode of sales']
    #type_com=self.extract_type_of_invoice(text ,my_dict,my_vat)
    type_NB = bank_or_nakd(text, excluded_words)
    #print(type_NB)             
    #print(f"the type of the invoice is {type_com}")
    #Ar_keyword = ["رقم حساب بنك","بنك"]
    #En_keyword = ["bank"]
    if type_NB == "The invoice type is: Bank":
        all_matches = extract_bank_info(text, excluded_words)
        if all_matches:
            print("Banks and Accounts:\n ***************\n")
            for match in all_matches:
                print("Bank Name:", match[0])
                print("Account Number:", match[1])
                print("-"*16)
                print()  # Empty line for readability
    invoice_num= inv_num(ttext,[vat1, vat2])
    #print(" invoice num: ",invoice_num)
    if invoice_num is None:
        inv = fix_output_txt_gemini(f"ما هو رقم الفاتورة   (invoice number): بهذا النص {text}",GOOGLE_API_KEY)
        if inv is not None:
            if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in inv) and inv:
                inv = arabic_to_english_numbers(inv)
                inv = arabic_to_english(inv)
        if 'لم' in inv or 'لا' in inv or 'غير' in inv or 'no' in inv or 'not' in inv or 'ليس' in inv or 'ليست' in inv:
            inv = None
            inv= fix_output_img_gemini('رقم الفاتورة',path,GOOGLE_API_KEY)
            if 'رقم الفاتورة هو' in inv or 'رقم الفاتورة' in inv or ':رقم الفاتورة هو' in inv:
                #inv= inv.replace(phrase, '')
                for phrase in phrase_list:
                    #inv = re.sub(r'\b{}\b'.format(phrase), '', inv)
                    inv = re.sub(r'\b{}\b\s*'.format(re.escape(phrase)), '', inv)
                #print("invoice number that found: ",inv)
                if inv is not None:
                    if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in inv) and inv:
                        inv = arabic_to_english_numbers(inv)
                        inv = arabic_to_english(inv)
            else:
                inv= inv
                if inv is not None:
                    if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in inv) and inv:
                        inv = arabic_to_english_numbers(inv)
                        inv = arabic_to_english(inv)
                #print("invoice number that found: ",inv)
        else:
            if 'رقم الفاتورة هو' in inv or 'رقم الفاتورة' in inv or ':رقم الفاتورة هو' in inv:
                #phrase= 'رقم الفاتورة هو' inv= inv.replace(phrase, '')
                for phrase in phrase_list:
                    #inv = re.sub(r'\b{}\b'.format(phrase), '', inv)
                    inv = re.sub(r'\b{}\b\s*'.format(re.escape(phrase)), '', inv)
                if inv is not None:
                    if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in inv) and inv:
                        inv = arabic_to_english_numbers(inv)
                        inv = arabic_to_english(inv)
                #print("invoice number that found: ",inv)
            else:
                inv = inv
                #print("invoice number that found: ",inv)
                if inv is not None:
                    if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in inv) and inv:
                        inv = arabic_to_english_numbers(inv)
                        inv = arabic_to_english(inv)
    #print("\n --------------------------------------------------------------------------------------------------------------\n")
    taxes =extract_tax_numbers(text,[total,subtotal,vat1,vat2])
    #print(taxes)
    #print("t: ", taxes)
    if taxes == (15.0 or 15):
        taxss = fix_output_txt_gemini(f"ما هى قيمة الضريبة بتلك الفاتورة (tax amount): بهذا النص {text}",GOOGLE_API_KEY)
        taxss= re.sub(r'[^\d.]', '', taxss)
        if taxss and taxss != ('.' or ''):
            taxss= float(taxss)
            #print("y: ", taxss)
            if taxss == (15.0 or 15):
                taxes = 15.0
            else:
                if taxss !=(1515.0 or 151515.0 or 1515):
                    taxes = taxss
                else:
                    taxes= 15.0
        else:
            taxes= None
    #subtotal_amount,tax_amount,total= self.Tax_total(total)
    #print(f"taxes :{taxes} , precentage : {pre}")
    #larges,smalls,taxs=Tax_total(large,small,taxes,tax_type)
    #print(f"larges :{larges} , smalls : {smalls} , taxs :{taxs}")
    discount = extract_discount(text)
    #print("Discount: ", discount)
    equations = extract_accounting_results(total,subtotal, percentage, taxes, discount)
    #print("After the Accountings Calculations: \n",equations[0:3])
    #print("\n --------------------------------------------------------------------------------------------------------------\n")
    if equations[0] is None:
        subtotals = fix_output_txt_gemini(f" ما هي قيمة الإجمالي قبل الضريبة لهذه الفاتوة (subtotal amount): بهذا النص {text}",GOOGLE_API_KEY)
        subtotals= re.sub(r'[^\d.]', '', subtotals)
        if subtotals and subtotals!= ('.' or ''):
            subtotals = float(subtotals)
            #print("Subtotal that found: ",subtotals)
        else:
            subtotals = None
            subtotals= fix_output_img_gemini('قيمة الإجمالي قبل الضريبة ',path,GOOGLE_API_KEY)
            subtotals= re.sub(r'[^\d.]', '', subtotals)
            #print("Subtotal that found: ",subtotals)
    if equations[1] is None:
        taxs = fix_output_txt_gemini(f"ما هى قيمة الضريبة بتلك الفاتورة (tax amount): بهذا النص {text}",GOOGLE_API_KEY)
        taxs= re.sub(r'[^\d.]', '', taxs)
        if taxs and taxs!= ('.' or ''):
            taxs = float(taxs)
            #print("Tax that found: ",taxs)
        else:
            taxs= None
            taxs= fix_output_img_gemini('قيمة الضريبة',path,GOOGLE_API_KEY)
            taxs= re.sub(r'[^\d.]', '', taxs)
            #print("Tax that found: ",taxs)
    if equations[2] is None:
        totals = fix_output_txt_gemini(f"ما هى قيمة الإجمالي بعد الضريبة لهذه الفاتورة (total amount): بهذا النص {text}",GOOGLE_API_KEY)
        totals= re.sub(r'[^\d.]', '', totals)
        if totals and totals!= ('.' or ''):
            totals = float(totals) 
            #print("Total that found: ",totals)
        else:
            totals= None
            totals= fix_output_img_gemini('قيمة الإجمالي بعد الضريبة',path,GOOGLE_API_KEY)
            totals= re.sub(r'[^\d.]', '', totals)
            #print("Total that found: ",totals)
    #print("\n --------------------------------------------------------------------------------------------------------------\n")
    date,due_date,date_american, due_date_american = extract_dates(text)
    #print(f"date :{date}   \n       due date :{due_date}")
    #print("\n --------------------------------------------------------------------------------------------------------------\n")
    if date is None:
        dates = fix_output_txt_gemini(f"ما هو التاريخ و تاريخ الاستحقاق (date & due date): بهذا النص {text}",GOOGLE_API_KEY)
        if 'لم' in dates or 'لا' in dates or 'غير' in dates or 'no' in dates or 'not' in dates or 'ليس' in dates or 'ليست' in dates:
            date= None
            due_date= None
            dates = fix_output_img_gemini('التاريخ و تاريخ الاستحقاق',path,GOOGLE_API_KEY)
            #print("Dates that found: ",dates)
            if 'لم' in dates or 'لا' in dates or 'غير' in dates or 'no' in dates or 'not' in dates or 'ليس' in dates or 'ليست' in dates:
                date,due_date,date_american, due_date_american =extract_dates(dates)
                #print(f"date :{date}   \n       due date :{due_date}")
            else:
                date= None
                due_date= None
        else:
            dates = dates
            #print("Dates that found: ",dates)
            date,due_date,date_american, due_date_american =extract_dates(dates)
            #print(f"date :{date}   \n       due date :{due_date}")
    #############################################################################################################################
    #print("\n --------------------------------------------------------------------------------------------------------------\n")
    #print("\n --------------------------------------------------------------------------------------------------------------\n")
    if invoice_num is not None:
        invoice_num = re.sub(r'[.:]', '', invoice_num)
        inv_nums = invoice_num
    elif inv is not None:
        inv = re.sub(r'[.:]', '', inv)
        inv_nums = inv
    else:
        inv_nums = None
    #if company_name is not None:
        #comp_names = company_name
    #elif comps is not None:
        #comp_names = comps
    #else:
        #comp_names = None
    if vat1 is not None:
        vat_nums = vat1
    elif vats is not None:
        vat_nums = vats
    else:
        vat_nums = None
    if equations[0] is not None:
        sub_total= equations[0]
    elif subtotals is not None:
        sub_total= subtotals
    else:
        sub_total= None
                
    if equations[1] is not None:
        tax_nums= equations[1]
    elif taxs is not None:
        tax_nums= taxs
    else:
        tax_nums= None   
        
    if equations[2] is not None:
        total_nums= equations[2]
    elif totals is not None:
        total_nums= subtotals
    else:
        total_nums= None
    #print(f"invoice number {inv_nums}\n company names: {comp_names}\n vat number: {vat_nums}\n date: {date}, due date: {due_date} \n date_american: {date_american}, due_date_american: {due_date_american} \nsubtotal: {sub_total}\n taxes: {tax_nums}\n total: {total_nums}")
    return [inv_nums,  vat_nums,type_NB, date ,due_date,date_american, due_date_american,type_com,sub_total,tax_nums,total_nums]
    