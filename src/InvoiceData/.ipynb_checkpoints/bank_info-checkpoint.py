import re
import regex as re

def extract_bank_info(text, excluded_words):
    matches = []
    Ar_keyword = ["رقم حساب بنك","بنك"]
    En_keyword = ["bank"]
    # Regular expression patterns
    arabic_pattern = re.compile(r'({})\s+(\S+)\s*([A-Za-z]*\d{{5,}}[A-Za-z]*)?'.format('|'.join(Ar_keyword)))
    english_pattern = re.compile(r'(\S+)\s+({})\s*([A-Za-z]*\d{{5,}}[A-Za-z]*)?'.format('|'.join(En_keyword)))
    # Iterate through each line in the text
    for line in text.split('\n'):
        if any(word in line.lower() for word in excluded_words):
            continue  # Skip this line if it contains any excluded word
        # Search for Arabic keywords
        arabic_matches = arabic_pattern.findall(line)
        for match in arabic_matches:
            Bank_name = match[1]
            account = match[2]
            matches.append((Bank_name, account))
        
        # Search for English keywords
        english_matches = english_pattern.findall(line)
        for match in english_matches:
            Bank_name = match[0]
            account = match[2]
            matches.append((Bank_name, account))
    
    return matches