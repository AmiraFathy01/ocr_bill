import re
import regex as re
from InvoiceData.bank_info import extract_bank_info
def bank_or_nakd(text, excluded_words):
    # Define trigger words
    trigger_words = ['type', 'النوع','الدفع','payment type', 'pay type','cust.','customer','العميل','اسم العميل','نوع الفاتورة','نوع','فاتورة','نوع السداد', 'نوع البيع','pay terms','فاتورة الى','فاتورة مبيعات','payment','طريقة الدفع','mode of sales']
    #excluded_words = ['type', 'النوع','الدفع','payment type', 'pay type','cust','customer','العميل','اسم العميل','نوع الفاتورة','نوع','فاتورة','نوع السداد', 'نوع البيع','pay terms','فاتورة الى','فاتورة مبيعات','payment','طريقة الدفع','mode of sales']
    # Define patterns for cash and bank
    cash_pattern = re.compile(r'\b(cash|cash sales invoice|cash invoice|نقدا|نقدية|نقدى|نقدي|عميل نقدى|عميل نقدي|فاتورة نقدية|نقدى|نقديا)\b', re.IGNORECASE)
    bank_pattern = re.compile(r'\b(bank|بنك)\b', re.IGNORECASE)

    # Iterate through trigger words
    # Split text into lines
    lines = text.split('\n')
    # Iterate through each line
    for line in lines:
        # Iterate through trigger words
        for word in trigger_words:
            # Find the position of trigger word in the line
            trigger_index = line.find(word)
            if trigger_index != -1:
                # Extract the substring after the trigger word
                substring = line[trigger_index + len(word):]
                # Search for cash and bank patterns
                if cash_pattern.search(substring):
                    return "Cash"
                elif bank_pattern.search(substring):
                    return "Bank", extract_bank_info(text, excluded_words)
    # If trigger words not found, search for cash pattern
    if cash_pattern.search(text):
        return "Cash"
    return "No type in this invoice shown"