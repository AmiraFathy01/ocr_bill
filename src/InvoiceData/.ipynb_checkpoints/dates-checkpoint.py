from datetime import datetime
from dateutil.parser import parse
from datefinder import find_dates
from datefinder import find_dates
import regex as re
from dateutil.parser import ParserError
import requests
import re

# this function try to see if date by the desired format or not and if no it convert it and print if also there is dates by american format
#also if there is date we are not sure what is the right format to it print it the 2 formats
def convert_date_format(date_str):
    date_american= None
    due_date_american= None
    if date_str is None:
        return None, None, None
    # Check if the date is already in the desired format based on the second number (month)
    match = re.match(r"(\d{1,2})\s*[/|-]\s*(\d{1,2})\s*[/|-]\s*(\d{4})", date_str)
    if match:
        _, month, _ = match.groups()
        if int(month) > 12:
            return date_str, None, None
    
    # Convert "5/2/2020" to "2020/2/5"
    match = re.match(r"(\d{1,2})\s*[/|-]\s*(\d{1,2})\s*[/|-]\s*(\d{4})", date_str)
    #american_execution = None
    #29/2/2024   2/29/2024
    if match:
        day, month, year = match.groups()
        if int(month)<= 12 and int(day)<= 12:
            date_american = f"{year}-{day}-{month}"
            due_date_american = f"{year}-{day}-{month}"
            #american=[date_american,due_date_american]
            #print("American and British Dates and Due Dates:\n ******************************\n")                
            #print(f"Date by American format: {date_american}, Due Date by American format: {due_date_american}")

        #day, month, year = match.groups()
        return f"{year}/{month}/{day}", date_american, due_date_american
    return date_str, None, None
######################
# this function extract all dates from text
def extract_dates(text):
    date = None
    due_date = None
    date_american = None
    due_date_american = None
    matches_arabic = []  # Define an empty list before using it
    extracted_dates = []
    #american_execution = None
    words_before_date = ["issue", "quote", "start", "start of", "statement", "approved by", "from", "delivery", "arrival", "departure", "ship", "invoice", "receipt", "today's", "today", "order"]
    exceptions_for_due_date = ["due", "due by", "end by", "to", "end", "end of"]
    date_words = ["due by", "start by", "end by", "ordered on", "approved by", "ordered by", "order on", "order by", "arrival", "departure"]
    date_words_arabic = [
        "اصدار الفاتورة", "اصدار", "اصدرات فى", "تاريخ الاستحقاق", 
        "تاريخ التسليم", "بتاريخ", "تحريرا فى", "من", "الى", "إلى", 
        "الاصدار", "الانتهاء", "الموافق", "التاريخ الموافق", 
        "تاريخ المستند", "تاريخ التسجيل", "تاريخ الانتهاء", 
        "تاريخ الاصدار", "تاريخ", "التاريخ", "تاريخ التحرير", 
        "تاريخ الفاتورة", "تاريخ التوريد","إصدار","الإصدار",
        "اصدارالفاتوره","إصدارالفاتوره", "اصدار الفاتوره","إصدار الفاتوره","اصدار الفاتورة",
        "إصدار الفاتورة","اصدارات في"," إصدارات في","إصدارات فى","تاريخ الإستحقاق",
        "تحرير في", "تحريرا في","تحريرا فى","تحريراً في","تحريراً فى",
        "الإنتهاء","تاريخ الإنتهاء","تاريخ الإصدار","تاريخ الفاتوره",    ]

    date_patterns = [
         #(0[1-9]|[12][0-9]|30|31) DD
        # (0[1-9]|1[0-2]) MM
        #(2[0-9][0-9][0-9][0-9]|19[0-9]) YYYY (1900-2999)
        # 1- month word(English)
            ################################
            r"\d{1,2}\s*-\s*(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\s*-\s*\d{2,4}",
            r"(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[-,\s.]*\d{1,2}(st|nd|rd|th)?[-,\s.]*\d{4}",
            r"\d{1,2}(st|nd|rd|th)?[-,\s.]*(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[-,\s.]*\d{4}" ,
            r"\d{4}[-,\s.]\d{1,2}(st|nd|rd|th)?[-,\s.]*(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)" ,
            r"\d{4}[-,\s.]*(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[-,\s.]*\d{1,2}(st|nd|rd|th)?" ,
            r"\d{1,2}\s*-\s*(january|february|march|april|may|june|july|august|september|october|november|december)\s*-\s*\d{2,4}",
            r"(january|february|march|april|may|june|july|august|september|october|november|december)[-,\s.]*\d{1,2}(st|nd|rd|th)?[-,\s.]*\d{4}",
            r"\d{1,2}(st|nd|rd|th)?[-,\s.]*(january|february|march|april|may|june|july|august|september|october|november|december)[-,\s.]*\d{4}",
            r"\d{4}[-,\s.]*\d{1,2}(st|nd|rd|th)?[-,\s.]*(january|february|march|april|may|june|july|august|september|october|november|december)",
            r"\d{4}[-,\s.]*(january|february|march|april|may|june|july|august|september|october|november|december)[-,\s.]*\d{1,2}(st|nd|rd|th)?"
            ################################

        # 2- month word(arabic)
            r"(يناير|فبراير|مارس|أبريل|ابرايل|ابريل|إبرايل|مايو|يونيو|يوليو|أغسطس|اغسطس|إغسطس|سبتمبر|أكتوبر|اكتوبر|إكتوبر|نوفمبر|ديسمبر)[-,\s.]*\d{1,2}[-,\s.]*\d{4}",
            r"\d{1,2}[-,\s.]*(يناير|فبراير|مارس|أبريل|ابرايل|ابريل|إبرايل|مايو|يونيو|يوليو|أغسطس|اغسطس|إغسطس|سبتمبر|أكتوبر|اكتوبر|إكتوبر|نوفمبر|ديسمبر)[-,\s.]*\d{4}" ,
            r"\d{4}[-,\s.]*\d{1,2}[-,\s.]*(يناير|فبراير|مارس|أبريل|ابرايل|ابريل|إبرايل|مايو|يونيو|يوليو|أغسطس|اغسطس|إغسطس|سبتمبر|أكتوبر|اكتوبر|إكتوبر|نوفمبر|ديسمبر)" ,
            r"\d{4}[-,\s.]*(يناير|فبراير|مارس|أبريل|ابرايل|ابريل|إبرايل|مايو|يونيو|يوليو|أغسطس|اغسطس|إغسطس|سبتمبر|أكتوبر|اكتوبر|إكتوبر|نوفمبر|ديسمبر)[-,\s.]*\d{1,2}" ,
       # 3- numbers
            # YYYY  in the first  and month and date
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?/\s?(0?[1-9]|1[0-2])\s?/\s?([12][0-9]|30|31|0?[1-9])" ,   # YYYY/MM/DD
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?\\s? (0?[1-9]|1[0-2])\s?\\s?([12][0-9]|30|31|0?[1-9])" ,    # YYYY\MM\DD
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?-\s?(0?[1-9]|1[0-2])\s?-\s?([12][0-9]|30|31|0?[1-9])" ,   # YYYY-MM-DD
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?\.\s?(0?[1-9]|1[0-2])\s?\.\s?([12][0-9]|30|31|0?[1-9])",       # YYYY.MM.DD


          # YYYY  in the first and date and month 
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?/\s?([12][0-9]|30|31|0?[1-9])\s?/\s?(0?[1-9]|1[0-2])" ,   # YYYY/dd/mm
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?\\s? ([12][0-9]|30|31|0?[1-9])\s?\\s?(0?[1-9]|1[0-2])" ,    # YYYY\dd\mm
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?-\s?([12][0-9]|30|31|0?[1-9])\s?-\s?(0?[1-9]|1[0-2])" ,   # YYYY-dd-mm
            r"(2[0-9][0-9][0-9]|19[0-9][0-9])\s?\.\s?([12][0-9]|30|31|0?[1-9])\s?\.\s?(0?[1-9]|1[0-2])",       # YYYY.dd.mm

            #DD in the first
            r"([12][0-9]|30|31|0?[1-9])\s?/\s?(0?[1-9]|1[0-2])\s?/\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,    # DD/MM/YYYY
            r"([12][0-9]|30|31|0?[1-9])\s?\\s?(0?[1-9]|1[0-2])\s?\\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,   # DD\MM\YYYY
            r"([12][0-9]|30|31|0?[1-9])\s?-\s?(0?[1-9]|1[0-2])\s?-\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,    #  DD-MM-YYYY
            r"([12][0-9]|30|31|0?[1-9])\s?\.\s?(0?[1-9]|1[0-2])\s?\.\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,  # DD.MM.YYYY

           # MM in the first
            r"(0?[1-9]|1[0-2])\s?/\s?([12][0-9]|30|31|0?[1-9])\s?/\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,    # MM/DD/YYYY
            r"(0?[1-9]|1[0-2])\s?\\s?([12][0-9]|30|31|0?[1-9])\s?\\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,   # MM\DD\YYYY
            r"(0?[1-9]|1[0-2])\s?-\s?([12][0-9]|30|31|0?[1-9])\s?-\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,    # MM-DD-YYYY
            r"(0?[1-9]|1[0-2])\s?\.\s?([12][0-9]|30|31|0?[1-9])\s?\.\s?(2[0-9][0-9][0-9]|19[0-9][0-9])" ,  # MM.DD.YYYY  
            # YYYY/MM/DD using Arabic numerals
            r"(١[0-9]{3}|٢[0-9]{3})[/\s](٠[1-9]|١[0-2])[/\s](٠[1-9]|[12][0-9]|٣[01])",
            # MM/DD/YYYY using Arabic numerals
            r"(٠[1-9]|١[0-2])[/\s](٠[1-9]|[12][0-9]|٣[01])[/\s](١[0-9]{3}|٢[0-9]{3})",
            # DD/MM/YYYY using Arabic numerals
            r"(٠[1-9]|[12][0-9]|٣[01])[/\s](٠[1-9]|١[0-2])[/\s](١[0-9]{3}|٢[0-9]{3})",
        
            r"\b\d{1,2}\s*[-/]\s*\d{1,2}\s*[-/]\s*(?!1|١)\d{4}\b",
            r"\b(?!1|١)\d{4}\s*[-/]\s*\d{1,2}\s*[-/]\s*\d{1,2}\b",            
        
            r"(٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){4}",
            r'(٠|١|٢|٣|٤|٥|٦|٧|٨|٩){4}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}',
            r"(٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){2}",
            r'(٠|١|٢|٣|٤|٥|٦|٧|٨|٩){2}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}[/\s](٠|١|٢|٣|٤|٥|٦|٧|٨|٩){1,2}',
            r"\b\d{1,2}\s*[-/]\s*\d{1,2}\s*[/-]\s*(2|٢)\d\b",# 22-02-23 OR 24/01/11
            r"\b(2|٢)\d\s*[-/]\s*\d{1,2}\s*[-/]\s*\d{1,2}\b", # 22-02-23 OR 24/01/11  
            r"\b\d{1,2}\s*[-/]\s*\d{1,2}\s*[-/]\s*\d{1,2}\b"

    ]#'٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' (٠|١|٢|٣|٤|٥|٦|٧|٨|٩)
    # search line by line for dates and add it to extracted_dates list
    for line in text.split("\n"):
        for pattern in date_patterns:
            match = re.search(pattern, line.lower())
            if match:
                extracted_dates.append(match.group(0))
                break

    if extracted_dates:
        # Check if date and due_date are not None and if the lists are not empty before accessing their elements
        # date will be the first date found list and due date the second if more than one date found in the list
        if len(extracted_dates) > 1:
            date, due_date = extracted_dates[0], extracted_dates[1]
        else:
            # only one date found in extracted_dates it will be the date
            date = extracted_dates[0]
            #print("The Date is: ", date)
    #print(date)
     # Convert dates to a consistent format, and also see if there any american dates extracted from function convert_date_format
    date, date_american, _= convert_date_format(date)
    due_date,_, due_date_american = convert_date_format(due_date)
      # print("\n --------------------------------------------------------------------------------------------------------------")
       #print(" \nPlease if there anything not accurate, reupload your file and make sure to make the text good and clear for satisfactory results for you \n رجاءا إذا كان هناك شيء غير دقيق، أعد تحميل الملف مرة أخرى وتأكد من جعل النص واضحًا لنتائج مرضية لك ")

         # change the arabic date to the month by number 
    date_list_arabic =['يناير', 'بنابر', 'ينابر', 'بناير', 'فبراير', 'فبرابر', 'فيراير', 'فيرابر', 'مارس', 'أبريل',
                       'ابريل', 'أبربل', 'أيريل', 'أيربل', 'ابربل', 'ايريل', 'ايربل', 'مايو', 'مابو',
                       'يونيو', 'بونيو', 'بونبو', 'يونبو', 'يوتيو', 'بوتيو', 'بوتبو', 'يوتبو', 'يوليو', 'يولبو', 'بوليو',
                       'بولبو', 'أغسطس', 'اغسطس', 'سبتمبر', 'سيتمبر', 'سيتمير', 'سبتمير', 'سبنمبر', 'سينمبر',
                       'سينمير', 'سبنمير', 'أكتوبر', 'اكتوبر', 'أكتوير', 'أكنوير', 'أكنوبر', 'اكتوير', 'اكنوير',
                       'اكنوبر', 'نوفمبر', 'نوفمير', 'توفمبر', 'توفمير', 'ديسمبر', 'ديسمير', 'دبسمير', 'دبسمبر']



 #   date_arabic=  {"1":r"January|jan|بناير|يناير|بنابر|ينابر",
  #                 "2":r"February|feb|فيرابر|فيراير|فبراير|فبرابر",
   #                "3":r"مارس|mar|March",
    #               "4":r"ايريل|أبربل|ابريل|أبريل|أيريل|أيربل|ابربل|ايربل|apr|April",
     #              "5":r"مابو|مايو|may",
      #             "6":r"يوتبو|بوتبو|يوتيو|بونبو|بونيو|يونيو|يونبو|بوتيو|jun|June",
       #            "7":r"July|jul|يوتبو|بوليو|يوليو|يولبو|بولبو",
        #           "8":r"اغسطس|أغسطس|aug|August",
         #          "9":r"سبنمير|سبنمبر|سيتمير|سيتمبر|سبتمبر|سبتمير|سينمبر|سينمير|sep|September",
          #         "10":r"أكتوبر|اكتوبر|أكتوير|أكنوير|أكنوبر|اكتوير|اكنوير|اكنوبر|oct|October",
           #        "11":r"توفمبر|نوفمير|نوفمبر|توفمير|nov|November",
            #       "12":r"ديسمبر|ديسمير|دبسمير|دبسمبر|dec|December",}

    date_arabic=  { "يناير": 1,
                "بنابر": 1,
                "ينابر": 1,
                "بناير": 1,
                "فبراير": 2,
                "فبرابر": 2,
                "فيراير": 2,
                "فيرابر": 2,
                "مارس": 3,
                "أبريل": 4,
                "ابريل": 4,
                "أبربل": 4,
                "أيريل": 4,
                "أيربل": 4,
                "ابربل": 4,
                "ايريل": 4,
                "ايربل": 4,
                "مايو": 5,
                "مابو": 5,
                "يونيو": 6,
                "بونيو": 6,
                "بونبو": 6,
                "يونبو": 6,
                "يوتيو": 6,
                "بوتيو": 6,
                "بوتبو": 6,
                "يوتبو": 6,
                "يوليو": 7,
                "يولبو": 7,
                "بوليو": 7,
                "بولبو": 7,
                "يوتبو": 7,
                "أغسطس": 8,
                "اغسطس": 8,
                "سبتمبر": 9,
                "سيتمبر": 9,
                "سيتمير": 9,
                "سبتمير": 9,
                "سبنمبر": 9,
                "سينمبر": 9,
                "سينمير": 9,
                "سبنمير": 9,
                "أكتوبر": 10,
                "اكتوبر": 10,
                "أكتوير": 10,
                "أكنوير": 10,
                "أكنوبر": 10,
                "اكتوير": 10,
                "اكنوير": 10,
                "اكنوبر": 10,  
                "نوفمبر": 11,
                "نوفمير": 11,
                "توفمبر": 11,                
                "توفمير": 11, 
                "ديسمبر": 12,
                "ديسمير": 12,
                "دبسمير": 12,
                "دبسمبر": 12,}

    #for month in date_list_arabic:
     #   if date is not None and month in date:
      #      date = date.replace(month, str(date_arabic[month]))
         #   break

    #for month in date_list_arabic:
       # if due_date is not None and month in due_date:
         #   due_date = due_date.replace(month, str(date_arabic[month]))
          #  break
            #######################
    # this part see if there in the month found arabic month wrote by this way " يناير " to convert the moth to number with what is in front of it from numbers in date_arabic
    for month in date_list_arabic:
        if date is not None and month in date:
            date = re.sub(r'\b{}\b'.format(month), str(date_arabic[month]), date)
            break

    for month in date_list_arabic:
        if due_date is not None and month in due_date:
            due_date = re.sub(r'\b{}\b'.format(month), str(date_arabic[month]), due_date)
            break

#**************************************************** 

    # Convert dates to a consistent format
    try:
        if date is not None:
            date = parse(date).date()
        else:
            date = None
    except ParserError:
        # Handle the case where parsing fails
        print("SORRY: Failed to parse the date and you can try again")
        date = None
    try:
        if due_date is not None:
            due_date = parse(due_date).date()
        else:
            due_date = None
    except ParserError:
        # Handle the case where parsing fails
        print("SORRY: Failed to parse the due date and you can try again")
        due_date = None

    # Ensure correct order of dates and due date is after date
    if date and due_date and date > due_date:
        due_date, date = date, due_date
    #print(due_date)
    #print(date_american)
    if due_date == None:     
        due_date = date
        due_date_american = date_american
    l_pattern = r"(\d{1,2}\s*[-/]\s*\d{1,2}\s*[-/]\s*)(\d{2,4})"
    if date is not None:
        ddate_str = date.strftime("%d-%m-%Y")   
        match = re.match(l_pattern, ddate_str)
        if match:
            day_month_part = match.group(1)
            year_part = match.group(2)
            if len(year_part) == 2:
                year_part = "20" + year_part
                date = day_month_part + year_part
                date = srt(date)
                #day_month_parts = '20' + day_month_part
                #date_american = day_month_parts + year_part
  
    if due_date is not None:
        ddue_date_str = due_date.strftime("%d-%m-%Y")
        match2 = re.match(l_pattern, ddue_date_str)
        if match2:
            day_month_part = match2.group(1)
            year_part = match2.group(2)
            if len(year_part) == 2:
                year_part = "20" + year_part
                due_date = day_month_part + year_part
                due_date = str(due_date)
                #day_month_parts = '20' + day_month_part
                #due_date_american = day_month_parts + year_part  
    #print(f"date: {date}, due_date: {due_date}")

    #if due_date == None:     
        #due_date = date
    #print(f"Date by British format:-  {date}, Due Date by British format:- {due_date}") 
    

    # if there is american dates to print the american and british date in list to user to choose from it and the same in due date
    if date_american and date:
        #print("-"*20)
        #print("Choose The Right Date: ")
        date_american = date_american
        date = str(date)
        #print([date_american, str(date)])
    if due_date_american and due_date:
        #print("Choose The Right Due Date: ")
        due_date_american = due_date_american
        due_date = str(due_date)
        #print([due_date_american, str(due_date)])
    if date:
        date = str(date)
    if due_date:
        due_date = str(due_date)
    return date, due_date, date_american, due_date_american
