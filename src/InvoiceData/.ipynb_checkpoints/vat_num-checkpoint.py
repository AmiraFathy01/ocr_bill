import re
import regex as re
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.convert_arabic_numbers import arabic_to_english_numbers, arabic_to_english
# this function to extract vat number from text ( company and client vat), and it excute only if some rules in function extract_company_vat ( if conditions) not match
# it returns 2 lists vat1_list for company, vat2_list for client
def extract_vat(text): 
    text = text.lower() 
    # list for words to search to match
    vat1 =  ['رقم ضريبة', 'رقم ضريبى', 'الرقم الضريبى للشركة', 'الرقم الضريبي '
                        , 'الرقم الضريبى', 'الرقم الضريبي','الرقم الضرييى','الرقم الضرييي', 'رقم ضريبى', 'رقم ضريبي', 'رقم ضريبي ','vat' ,'رقم الضريبة','رقم ضريبة'] 
    # phrase to don't matchs
    exclude_phrase = ['client', 'customer', 'supplier','للعميل','total','tax']
    # pattern exclude
    pattern_exclude = re.compile(fr'\b(?:{"|".join(map(re.escape, exclude_phrase))})\b', flags=re.IGNORECASE)
    # pattern to match 
    pattern = re.compile(fr'\b(?:{"|".join(map(re.escape, vat1))})\b', flags=re.IGNORECASE)
    # pattern to match number  
    number_pattern = r'\b(?P<number>(\d\s?){15})\b'
    #number_pattern = r'(?P<number>\d{6}(?:\s?\d{3}){1,3}(?:,\d*)?(?:\.\d*)?(?:\.\d*)?)'
    # chars of arabic and english
    character_set = r'[آإةأاىبجدهوزحطيكلمنسعفصقرشتثخذضظغؤئ|\s|a-zA-Z]*' 
    vat1_list = []
    vat2_list = []
    for line in text.split('\n'):
        matches = re.search(pattern, line)
        if matches:
            exclude = re.search(pattern_exclude, line)
            if not exclude:
                match_numbers = re.findall(number_pattern, line, re.IGNORECASE)
                for match in match_numbers:
                    match = match[0]
                    match = match.replace(" ", "")  # Remove spaces between numbers
                    if '.' not in match:
                        vat1_list.append(match)
            else:
                match_numbers = re.findall(number_pattern, line, re.IGNORECASE)
                for match in match_numbers:
                    match = match[0]
                    match = match.replace(" ", "")  # Remove spaces between numbers
                    if '.' not in match:
                        vat2_list.append(match)
                        #print(f"client vat number: {match_numbers}")

    # Remove duplicates from the list
    vat1_list = list(set(vat1_list))
    vat2_list = list(set(vat2_list))

    # Remove numbers in the company VAT list that are also in the client VAT list
    #print(f"set vat  = {vat1_list}")
    vat1_list = list(set(vat1_list) - set(vat2_list))
    #print(vat1_list)
    #print(vat2_list)
    if len(vat1_list) >=2 :
        if len(vat2_list)==0:
            vat2_list= vat1_list[1]  
            vat1_list =vat1_list[0]
        else :
            vat1_list =vat1_list[0]
            vat2_list=vat2_list[0]

    elif len(vat1_list) ==1:
        if len(vat2_list)==0:
            vat1_list= vat1_list[0] 
            vat2_list =None
        else :
            vat1_list = vat1_list[0]
            vat2_list = vat2_list[0]
    else :
        vat1_list=None
        vat2_list =None
    # If vat1_list and vat2_list are still None, search for 15-digit numbers in the text , the search here not depend on any words to search
    if vat1_list is None and vat2_list is None:
        pattern = r'\b\d{15}\b'
        matches = re.findall(pattern, text)
        if matches:
            if len(matches) == 2:
                vat1_list, vat2_list = matches[0], matches[1]
            elif len(matches) == 1:
                vat1_list, vat2_list = matches[0], matches[0]
            else:
                vat1_list = None
                vat2_list = None
    
    # Check for Arabic numbers in vat1_list or vat2_list and convert them if found
    if vat1_list is not None:
        if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in vat1_list) and vat1_list:
            vat1_list = arabic_to_english_numbers(vat1_list)
            vat1_list = arabic_to_english(vat1_list)
    if vat2_list is not None:
        if any(char in '٠١٢٣٤٥٦٧٨٩' or '۰۱۲۳۴۵۶۷۸۹' for char in vat2_list) and vat2_list:
            vat2_list = arabic_to_english_numbers(vat2_list)    
            vat2_list = arabic_to_english(vat2_list)
            if vat2_list == vat1_list:
                vat2_list= None
    #print(f"first  vat {vat1_list}")
    #print(f"second  vat {vat2_list}")                
    #if vat1_list is None and vat2_list is None:
        #text_lines = text.split('\n')[:15]
        #vat1_list, vat2_list = self.extract_vat('\n'.join(text_lines))
    return vat1_list, vat2_list 