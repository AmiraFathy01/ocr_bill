#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import numpy as np 


## import  Functions 


def find_horizontal_line(coordinates):
        """
        الدالة لإيجاد الخط الأفقي الذي يقطع مجموعة من الخطوط العمودية.
        :param coordinates: قائمة تحتوي على الإحداثيات بصيغة (x1, y1, x2, y2) لكل خط عمودي.
        :return: قيمة y للخط الأفقي المحسوب.
        """
        # تجميع جميع قيم y للنقاط العلوية والسفلية للخطوط العمودية
        y_values = []
        x_values=[]
        for coord in coordinates:
            # افتراض أن coord[1] و coord[3] هما النقاط العلوية والسفلية للخط العمودي
            if coord :
                coord=coord[0]
                y_values.append(coord[1])
                y_values.append(coord[3])
                x_values.append(coord[0])
                x_values.append(coord[2])
        # حساب القيمة المتوسطة لجميع قيم y، والتي ستمثل موقع الخط الأفقي
        try:
            y_mean = np.mean(y_values)
            x1,x2=min(x_values),max(x_values)
            return y_mean ,x1,x2
        
        except:
            return None,None,None

