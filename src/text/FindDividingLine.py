#!/usr/bin/env python
# coding: utf-8

# In[ ]:
import os,io 
import cv2 
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
from io import StringIO 
import re


## import  Functions 
## 

##

from text.FindHorizontalLine import find_horizontal_line
from text.FindLimitLine import find_limits_line

def find_dividing_line (img_path,table_header) :
        #image = cv2.imread(img_path)
        image =img_path
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (3, 3), 0)
        thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
        # Detect vertical lines
        vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 50))
        vertical_mask = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=1)
        # Find contours of vertical lines
        contours, _ = cv2.findContours(vertical_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # Draw the contours (optional)
        # cv2.drawContours(image, contours, -1, (0, 0, 255), 2)
        # Extract the coordinates of the vertical lines
        vertical_lines_coords = []
        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            vertical_lines_coords.append((x, y, x + w, y + h))
        near_line=[]
        near_line_temp=[]
        list_near_line=[] 
        for i in range(0, 1):
                if i <(len(table_header[0])-1):
                    set_header_points= table_header[0][i][1][2]
                    x1_point=set_header_points[2]
                    x1_curr=set_header_points[0]
                    x2_curr=set_header_points[2]
                    y1_point=set_header_points[1]
                    x2_point=table_header[0][i+1][1][2][0]
                    for idx, (x1, y1, x2, y2) in enumerate(vertical_lines_coords):
                        #if x1>=x1_point and   y1 in range (y1_point,y1_point+20) and  x2<x2_point :
                             if  y1 >=y1_point-50   and y1<= y1_point +600 :
                                        near_line.append([ x1, y1, x2, y2])
                                        near_line_temp.append( [x1, y1, x2, y2])
                                        cv2.line(image, (x1, y1), (x2, y2), (0, 0, 255), 2)

                else :
                    x1_point = x2_point 
                    for idx, (x1, y1, x2, y2) in enumerate(vertical_lines_coords):
                        #if x1>=x1_point and   y1 in range (y1_point,y1_point+20) and  x2<x2_point :
                        if x1>=x1_point and   y1 >=y1_point -50   :
                                        near_line.append([ x1, y1, x2, y2])
                                        near_line_temp.append( [x1, y1, x2, y2])
                                        cv2.line(image, (x1, y1), (x2, y2), (0, 0, 255), 2)
               # print("****************************************")
                list_near_line+=[near_line_temp]
                near_line_temp=[]
        #plt.imshow(image)
        #plt.title('Detected Lines')
        #plt.show()
        y1,x1,x2=find_horizontal_line(list_near_line)

        limit_x1_x2=find_limits_line(list_near_line,y1)
        
        y1,x1,x2=find_horizontal_line(list_near_line)
        if y1 != None :

            end_diving=int(y1)
            cv2.line(image, (0, end_diving), (image.shape[1], end_diving), (0, 0, 255), 2)
           
        else :
            end_diving =image.shape[0]
        if limit_x1_x2 != None:
            limit_x1_x2= sorted(list_near_line[0], key=lambda coord: coord[0])

        return near_line ,end_diving,limit_x1_x2

