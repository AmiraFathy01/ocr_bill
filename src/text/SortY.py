#!/usr/bin/env python
# coding: utf-8

# In[ ]:
## import library 
import os,io 
import cv2 
import numpy as np 

import pandas as pd 
from io import StringIO 
import re

## import  Functions 


from text.FindDividingLine import find_dividing_line

def sort_text_y(image_path,sorted_lines,word_header,space_num=12):

        line_dict={} # dict for the line  have the texts and box
        lines_list=[]  # list for all line 
        x, y = 0, 0  # Initialize x and y
        y1=0
        x2=0
        line = ""    # Initialize line
        text = ""    # Initialize the list to store lines
        table_header=[]   # the header of table in invoice 
        line_number=0
        end_diving = None    # the  end of table 
        texts_table  =" "    # the text in table 
        lines_table=[]
        line_header_table=0
        start_table=None 
        limit_x1_x2=None
        found_word_header = 0 
        for index, ((x_coordinate, y_coordinate), [(texts, (top_left, top_right, bottom_right, bottom_left))]) in enumerate(sorted_lines[:-1]):
            texts= texts.lower()
            # for the begin the for loop 
            if x == 0 and y == 0:
                line += " " + texts  # add the texts to the line 
                text += "\n" + line   # add the line to the text  
                # add the line to the line dict 
                line_dict[x]=[x_coordinate,texts,(top_left.x, top_left.y, top_right.x, bottom_left.y)] 

            # if the texts not the start of the invoice (check if  the differnt between last y and current y coordinate is  less than 15)
            elif y_coordinate - y >=0 and y_coordinate - y <= space_num :
                if texts=="(" or texts==")" or texts=="|" or texts =="||"or texts =="[" or texts =="]":
                    continue

                line += " " + texts
                line_dict[x]=[x_coordinate,texts,(top_left.x, top_left.y, top_right.x, bottom_left.y)]

            # if the texts not the start of the invoice (check if  the differnt between last y and current y coordinate is  more than 15)
            elif y_coordinate - y >space_num:
                # check if this line include the header word to check if it the table or no
                if word_header!=None:
                    if sum(word in line for word in word_header)>=3:
                        # if  have the word of header  add the line  with the boxes and texts to the list table header 
                        # not add the line only but add the line_dic becouse have the boxes 
                        #print(sum(word in line for word in word_header))
                        table_header.append(sorted(line_dict.items(), key=lambda item: item[1]))    # sorted it before add     
                        line_header_table=line_number
                        #get the y in next line 
                        next_top_left = sorted_lines[index+1][1][0][1][0]
                        #print(next_top_left)
                        # call the function to detect the dividing of the line in table 
                        # and get the end dividing of line (end of table )
                        # and get the all line  
                        near_line,end_diving,limit_x1_x2 =find_dividing_line(image_path,table_header) 
                 # sort line before add to text depended on the x coordinate 
                line_soted=sorted(line_dict.items(), key=lambda item: item[1])
                line_sort= " "
                for i in range(0, len(line_soted)):
                    line_sort+= " "+line_soted[i][1][1]
                # get the texts in tables  
                if end_diving !=None  :
                    texts_table += "\n" + line_sort
                    lines_table.append(line_soted)  # add the line dict in the lines table 
                text += "\n" + line_sort          #add the line in new  line in text 
                line = " " + texts               # add texts in the line  (not important now)
                lines_list.append(line_dict)      # add the line dict in the lines list 
                line_dict={}    # make the line dict empty 
                line_dict[x+1]=[x_coordinate+1,"\n"]       # add the new line empty   (not important  )
                lines_list.append(line_dict)               # add the line dict with the empty line to the lines list  (not important)
                line_dict={}                               # make the line dict empty 
                line_dict[x]=[x_coordinate,texts,(top_left.x, top_left.y, top_right.x, bottom_left.y)]  # add the text & box to the line dict 
                line_number+=1     
            x, y = x_coordinate, y_coordinate
            y1=top_right.y
        #text_without_parentheses = text.replace('(', '').replace(')', '')
        #print(table_header[0])
        line_soted=sorted(line_dict.items(), key=lambda item: item[1])
        line_sort= " "
        for i in range(0, len(line_soted)):
            line_sort+=""+line_soted[i][1][1] 
        #print(end_diving)
        #print(text)
        #print(limit_x1_x2)
        return text,lines_table,limit_x1_x2,texts_table

