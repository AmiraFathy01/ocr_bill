#!/usr/bin/env python
# coding: utf-8

# In[ ]:



def sort_line_x(text_ocr):
    text_lines = {}
    count = 0
    for text_annotation in text_ocr:
        if count != 0:
            vertices = text_annotation.bounding_poly.vertices
            text = text_annotation.description
            # Skip adding certain characters to text_lines
            #if text not in {'(', ')', '"', "'", '|', '[', ']'}:
            # Assuming top-left vertex provides X and Y coordinates
            (top_left, top_right, bottom_right, bottom_left) = vertices
            # Round the coordinates to group lines with similar coordinates
            x_coordinate = round((top_left.x + top_right.x)/2)
            y_coordinate = round((top_left.y + bottom_left.y))
            key = (x_coordinate, y_coordinate)

            if key not in text_lines:
                text_lines[key] = []
            else: 
               key = (x_coordinate +1, y_coordinate+1)
               text_lines[key] = []
            text_lines[key].append((text, vertices))
        count = 1
    #print("*********************************************************************************")
    sorted_lines = sorted(text_lines.items(), key=lambda x: (x[0][1]))
    return sorted_lines
        

