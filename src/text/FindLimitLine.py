#!/usr/bin/env python
# coding: utf-8

# In[ ]:


def find_limits_line(limit_line,y1):
    final_limit_line=[]
    for sublist in limit_line:
        if sublist:  # Check if the sublist is not empty
            x1_values = [coord[0] for coord in sublist if coord[1] <= y1]
            x2_values = [coord[2] for coord in sublist if coord[1] <= y1]
            min_x1 = min(x1_values)
            max_x2 = max(x2_values)
            final_limit_line.append([min_x1,max_x2])
    return final_limit_line   

