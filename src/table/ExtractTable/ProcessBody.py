#!/usr/bin/env python
# coding: utf-8

# In[ ]:
import re 
import pandas as pd

def add_cell_word_to_df(df, i, k, cell_word, add_cell_word):
        try:
            if not pd.isna(df.loc[i+1, df.columns[k]]):
                current_column_name = df.columns[k]
                if re.search(r'\b(|بيان|الوص|البـ|رقم الصنف|الصنف رقم|الصنف اسم|ال|البند|مان|الصنف اسم|item|part #|description|الوصف|ان|service type|الخدمة نوع|نوع الخدمة|البيان)\b', current_column_name, flags=re.IGNORECASE):
                    df.loc[i+1,df.columns[k]] += cell_word
                    add_cell_word = True
                else:
                    df.loc[i+1,df.columns[k]] = cell_word
                    add_cell_word = True
            else:
                df.loc[i+1,df.columns[k]] = cell_word
                add_cell_word = True
        except:
            df.loc[i+1,df.columns[k]] = cell_word
            add_cell_word = True

        return df, add_cell_word



def diff_between_line_x1_word(vertical_lines,x1_word):
        for line in vertical_lines :
            diff_line_x1_word= x1_word -line[0]
            if diff_line_x1_word in range(-5,15):
                return True  
        return False 

def change_coordinate(x1_x2_cord,k,new_value):
        x1_x2_cord[k][0] = new_value
        return x1_x2_cord



