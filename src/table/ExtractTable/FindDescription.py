#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import re  

def find_description(texts_table,keywords= ['description']):
        # Split the text into lines
         # Words to search for
        # Split the text into lines
        lines = texts_table.split('\n')
        # Iterate through each line
        for index, line in enumerate(lines):
            # Stop at line 2
            if index == 1:
                #print(line)
                # Check if any keyword exists in the line
                for keyword in keywords:
                    if re.search(rf'\b{keyword}\b', line, re.IGNORECASE):
                        return True
                return False
        # Return False if the loop completes without finding any keyword in line 2
        return False
    
def clean_text_table(texts_table):
        # Remove empty lines
        cleaned_text = '\n'.join(line for line in texts_table.split('\n') if line.strip())
        return cleaned_text


def text_tables_header_have_numbers(texts_table):
        # Split the row by spaces

        lines = texts_table.split('\n')
        row=lines[0]
        #print(row)
        row_values = row.split()

        # Count the number of values that are numbers
        count_numbers = sum(1 for value in row_values if value.replace('.', '', 1).isdigit())

        # Check if the count exceeds 5
        return count_numbers > 3




