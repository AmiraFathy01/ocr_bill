#!/usr/bin/env python
# coding: utf-8

# In[ ]:
import re
import pandas as pd 


## import  Functions 


##

from table.ExtractTable.ProcessHeaderBodyTable import extract_vertical_lines,initialize_word_variables,process_word_comparison
from table.ExtractTable.ProcessHeaderBodyTable import find_vertical_x,find_vertical_x_before_line,change_diff_to_pos,process_diff_condition


def header_df(lines_table,limit_x1_x2):
        if  lines_table  :
            vertical_lines=extract_vertical_lines(limit_x1_x2)
            #print(vertical_lines)
            # lines in images  
            cell_word=""
            x1_x2_cord=[]
            x1_word=0
            x2_word=0
            df = pd.DataFrame()
            lines_table[0] = [entry for entry in lines_table[0] if entry[1][1] not in ['.', ':','#','البنك','بنك']]
            x1_word=lines_table[0][0][1][2][0]          # the first word in the line (x1 for the first word in the header)
            for j in range(0, len(lines_table[0])):     # for every word in the line  
                    vetical_x, find_line, word, box, x2, add_cell_word,y2 = initialize_word_variables(lines_table, j,i=0)
                    if j<len(lines_table[0])-1:             # if not the last word 
                        next_box=lines_table[0][j+1][1][2]  # next box to compare between the x1 and x2
                        word_next=lines_table[0][j+1][1][1] 
                        #print(word_next)
                        x1_next, y1_word, diff = process_word_comparison(box, next_box, vertical_lines, x1_word, x2)
                        diff =change_diff_to_pos(diff,j)
                        cell_word, x1_word, x2 = process_diff_condition(diff, word, box, x1_word, x2,cell_word)
                        
                        if diff >6 or diff<0 :
                            cell_word+=word
                            if len (vertical_lines)>3:
                                for ind,line in enumerate(vertical_lines):
                                    #print(vertical_lines)
                                    #print(line)
                                    x1_line=line[0]
                                    x2_line=line[2]
                                    y1_line=line[1]
                                    # الفانكشن مش شغاله لسبب ما 
                                    if y1_line > y1_word +150:
                                        continue 
                                    else :
                                        # list of coord is empty (first column coord)
                                        if len(x1_x2_cord) == 0:
                                            vertical_x,find_line = find_vertical_x(x1_x2_cord, x1_word, x2, vertical_lines)
                                        
                                            break
                                        elif (x2 <= x1_line +2 or x2<=x2_line+2) and ((x1_next >= x1_line -2 )or (x1_next >= x2_line -2)) :
                                            ind_before_line= ind-1
                                            vertical_x, find_line = find_vertical_x_before_line(ind, find_line, vertical_lines, ind_before_line, x1_word)
                                            if x2_line_before >=vertical_x[0]:
                                                vertical_x_1=int((x2_line_before+x1_word)/2)
                                                vertical_x[0]=vertical_x_1 
                                            break   
                            if find_line:
                                x1_x2_cord.append(vertical_x)
                            else :    
                                x1_x2_cord.append([x1_word,x2])
                            x1_word=x1_next
                            if cell_word in df.columns: 
                                cell_word =":"
                            df[cell_word]=[]
                        
                            cell_word= " "
                            x2_word_before = x2
                            x2_line_before=x1_x2_cord[-1][1]
                            if x1_x2_cord :
                                x2_line_before=x1_x2_cord[-1][1]
                            else: 
                                x2_line_before=x2
                    else :
                            cell_word+=word
                            if x1_x2_cord :
                                x2_line_before=x1_x2_cord[-1][1]
                                vertical_x_1=int((x2_line_before+x1_word)/2)
                                x1_x2_cord.append([vertical_x_1,x2])
                            else:
                                x1_x2_cord.append([x1_word,x2])
                            #x1_x2_cord.append([x1_word,x2])
                            x1_word=x1_next
                            if cell_word in df.columns:
                                cell_word =cell_word+":"
                            df[cell_word]=[]
                            cell_word= " "
            return df,x1_x2_cord
        else: 
            df = pd.DataFrame()
            return df ,None

