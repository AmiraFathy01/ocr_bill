#!/usr/bin/env python
# coding: utf-8

# In[ ]:


def fix_coord(x1_x2_cord,df):
        indices_to_merge=[]
        new_coord=[]
        for i in range(len(x1_x2_cord) - 1, -1, -1):
            x, x2 = x1_x2_cord[i]
            if (x+20 in range (x1_x2_cord[i-1][0],x1_x2_cord[i-1][1])) or(x2 in range (x1_x2_cord[i-1][0],x1_x2_cord[i-1][1])) :
                indices_to_merge.append([i,i-1])
                new_column_name = df.columns[i] +df.columns[i-1]
                # دمج قيم العمودين في عمود واحد
                df[new_column_name] =  df.columns[i] + df.columns[i-1]
                # حذف العمودين الأصليين
                df.drop([df.columns[i], df.columns[i-1]], axis=1, inplace=True)

                # إدراج العمود الجديد في بداية الإطار البياناتي
                df.insert(i-1, new_column_name, df.pop(new_column_name))
        coordinates=x1_x2_cord
        # Merge specified indices while keeping larger x first and larger x2 second
        for idx2, idx1 in indices_to_merge:
            coordinates[idx1][0] = min(coordinates[idx1][0], coordinates[idx2][0])  # small  x
            coordinates[idx1][1] = max(coordinates[idx1][1], coordinates[idx2][1])  # Larger x2

        # Remove the redundant coordinates
        coordinates = [coordinates[i] for i in range(len(coordinates)) if i not in [idx for idx, _ in indices_to_merge]]

        return coordinates,df

