#!/usr/bin/env python
# coding: utf-8

# In[ ]:
import os,io 
import cv2 
import numpy as np 
from google.cloud import vision
from scipy.ndimage import interpolation as inter
import matplotlib.pyplot as plt 
import pandas as pd 
from io import StringIO 
import re
import warnings 
warnings.filterwarnings('ignore') 
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

## import  Functions 

## 

##

from table.ExtractTable.ProcessBody import add_cell_word_to_df,diff_between_line_x1_word,change_coordinate

from table.ExtractTable.ProcessHeaderBodyTable import extract_vertical_lines,initialize_word_variables,process_word_comparison
from table.ExtractTable.ProcessHeaderBodyTable import change_diff_to_pos,process_diff_condition



def cell_df(df,lines_table,x1_x2_cord,limit_x1_x2):
        try :
            vertical_lines=extract_vertical_lines(limit_x1_x2)
            cell_word=""
            x1_x2_cord=x1_x2_cord
            x1_word=0
            x2_word=0
            y1=0
            y2=0
            y2_before=0
            cut_df=False
            for i in range(1, len(lines_table)):            # i for every line 
                x1_word=lines_table[i][0][1][2][0]          # the first word in the line (x1 for the first word in the line)
                if i>1:
                    y2=lines_table[i][0][1][2][3]
                    y1=lines_table[i][0][1][2][1]
                    diff_y2s=y1-y2_before

                    if diff_y2s > 100 and cut_df == True  :
                        break
                    else:
                        cut_df == False
                for j in range(0, len(lines_table[i])):     # for every word in the line 
                    vetical_x, find_line, word, box, x2, add_cell_word ,y2= initialize_word_variables(lines_table, j,i)

                    if j<len(lines_table[i])-1:             # if not the last word 
                        next_box=lines_table[i][j+1][1][2]  # next box to compare between the x1 and x2
                        word_next=lines_table[i][j+1][1][1] 
                        x1_next, y1_word, diff = process_word_comparison(box, next_box, vertical_lines, x1_word, x2)
                        diff =change_diff_to_pos(diff,j)

                        cell_word, x1_word, x2 = process_diff_condition(diff, word, box, x1_word, x2,cell_word)

                        if diff >6 :
                            cell_word+=word
                            
                            for k in range(0,len(x1_x2_cord)):
                                x1_head, x2_head = x1_x2_cord[k][0], x1_x2_cord[k][1]
                                #print(x1_head,x2_head)
                                if k > len(df.columns):
                                    break
                                #print(f"x1_word {x1_word}   x2 {x2}, x1_head  {x1_head}")
                                #print(f"jhjhjhjh  cell word {cell_word}")
                                #if x1_word >= x1_head:
                                try :
                                        if k==len(x1_x2_cord)-1:
                                            df, add_cell_word =add_cell_word_to_df(df, i, k, cell_word, add_cell_word)
                                            break
                                except :
                                        break
                                if( x1_word in range (x1_head -1,x2_head +1))or ( x2 in range (x1_head -1,x2_head +1)):
                                    ##############
                                    df, add_cell_word =add_cell_word_to_df(df, i, k, cell_word, add_cell_word)
                                    break
                                   
                                elif k<len(x1_x2_cord)-1:
                                    next_x1_head, next_x2_head =x1_x2_cord[k+1][0], x1_x2_cord[k+1][1]
                                    if ( x1_word in range (next_x1_head -1,next_x2_head +1))or ( x2 in range (next_x1_head -1,next_x2_head +1)):              
                                        #print("::::FFFFFFFF")
                                        continue
                                        ###ممكن تتعدل 
                                    elif ( x1_word in range (x2_head ,next_x1_head +1)) or ( x2 in range (x1_head ,next_x2_head +1)):              
                                        #print("#####################")
                                        if x2>next_x2_head:
                                            continue

                                        elif diff_between_line_x1_word(vertical_lines,x1_word) :
                                            x1_x2_cord=change_coordinate(x1_x2_cord,k+1,x1_word)
                                            df, add_cell_word = add_cell_word_to_df(df, i, k+1, cell_word, add_cell_word)

                                            break

                                        else:
                                            df, add_cell_word = add_cell_word_to_df(df, i, k, cell_word, add_cell_word)
                                            break
                            #if add_cell_word ==False:
                                
                             #   print("False")

                            x1_word=x1_next
                            cell_word=" "

                    else :
                            cell_word+=word
                        # print(f"############$$$$$$$$ {cell_word}")
                            #print(f"x1_word {x1_word}, x2 {x2} ")
                            for k in range(0,len(x1_x2_cord)):
                                x1_head, x2_head = x1_x2_cord[k][0], x1_x2_cord[k][1]
                                #print(x1_word,x2)
                                #print(x1_head,x2_head)
                                #print("&&&&&&&&&&&&&&&&&&&&&&&&")
                                if k > len(df.columns)-1:
                                    #print("yes")
                                    #print(cell_word)
                                    break
                                if x1_word >= x1_head:
                                    if x2 <= x2_head:
                                        df, add_cell_word = add_cell_word_to_df(df, i, k, cell_word, add_cell_word)
                                        break
                                    elif x1_word<= x2_head:
                                        df, add_cell_word = add_cell_word_to_df(df, i, k, cell_word, add_cell_word)
                                        break
                                    try:
                                        if k==len(x1_x2_cord)-1:

                                            if not pd.isna(df.loc[i+1, df.columns[k]]):
                                            # Do something if the value exists
                                            #   print("Value exists!")
                                            #  print(f" value {df.loc[i+1, df.columns[k]]}")
                                            # print(df.columns[k])
                                                current_column_name = df.columns[k]
                                                if re.search(r'\b(مان|services|الوصف|الوص|البـ|رقم الصنف|الصنف رقم|الصنف اسم|ال|البند|item|part #|description|الوصف|ان|service type|الخدمة نوع|نوع الخدمة|البيان)\b', current_column_name, flags=re.IGNORECASE):
                                                    #    print('yes')
                                                    df.loc[i+1,df.columns[k]]+=cell_word
                                                    add_cell_word=True
                                                    break
                                            else :
                                                df.loc[i+1,df.columns[k]]=cell_word
                                                add_cell_word=True
                                                break
                                            break
                                    except :
                                        break
                                else:

                                    df.loc[i+1,df.columns[k]]=cell_word
                                    break
                            #x1_word=x1_next
                            cell_word=''
                y2_before=y2     
                if i <= len(df):
                    row = df.iloc[i-1]
                    nan_count = row.isna().sum()
                    if nan_count >=3 :
                        cut_df=True 
            return df,x1_x2_cord 
        except Exception as e:
            print("Error getting text table:")
            print(f"Error message: {str(e)}")
            return None, None

        #return df,x1_x2_cord  

