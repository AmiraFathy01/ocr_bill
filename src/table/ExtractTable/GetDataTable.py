#!/usr/bin/env python
# coding: utf-8

# In[ ]:
import os,io 
from io import StringIO 
import re

## import  Functions 
## 
from text.SortY import sort_text_y
##
from table.ExtractTable.FindDescription import find_description,clean_text_table,text_tables_header_have_numbers


def get_data (image_path,sorted_lines,word_header_ara,word_header_eng,space_num):
        text,lines_table,limit_x1_x2,texts_table=sort_text_y(image_path,sorted_lines,word_header_eng,space_num)
        if lines_table==[] :
            text,lines_table,limit_x1_x2,texts_table=sort_text_y(image_path,sorted_lines,word_header_ara,space_num)

        texts_table = clean_text_table(texts_table)
        while text_tables_header_have_numbers(texts_table)and space_num >=5:
            space_num-=1
            text,lines_table,limit_x1_x2,texts_table=sort_text_y(image_path,sorted_lines,word_header_eng,space_num)
            if lines_table==[] :
                text,lines_table,limit_x1_x2,texts_table=sort_text_y(image_path,sorted_lines,word_header_ara,space_num)

            texts_table = clean_text_table(texts_table)


        return text,lines_table,limit_x1_x2,texts_table
    # while description in second line  


def get_text_table(image_path,text, lines_table, limit_x1_x2, texts_table, sorted_lines, word_header_ara, word_header_eng, space_num,keywords= ['description']):
        texts_table = clean_text_table(texts_table)

        while find_description(texts_table,keywords=keywords) and space_num <=22:
            space_num += 1
            text, lines_table, limit_x1_x2, texts_table = sort_text_y(image_path,sorted_lines, word_header_eng, space_num)
            if lines_table == []:
                text, lines_table, limit_x1_x2, texts_table = sort_text_y(image_path,sorted_lines, word_header_ara, space_num)
            texts_table = clean_text_table(texts_table)
            res = find_description(texts_table,keywords=keywords)
        res = find_description(texts_table,keywords=keywords)

        return  lines_table, limit_x1_x2, texts_table,res

