#!/usr/bin/env python
# coding: utf-8

# In[ ]:
## import library 
import os,io 

from io import StringIO 
import re
import warnings 

#vertical_lines=limit_x1_x2
# header table
#############################################################################
#########################################################################
#############################################################################
#########################################################################

def extract_vertical_lines(limit_x1_x2):
        vertical_lines = []
        if limit_x1_x2 is not None:
            for v in limit_x1_x2:
                if v[0] < 5:
                    continue
                else:
                    if any(abs(v[0] - v_line[0]) < 10 for v_line in vertical_lines):
                        min_diff = float('inf')
                        min_v_line = None
                        for v_line in vertical_lines:
                            diff = abs(v[0] - v_line[0])
                            if diff < 10 and diff < min_diff:
                                min_diff = diff
                                min_v_line = v_line
                        if min_v_line is not None and v[1] < min_v_line[1]:
                            vertical_lines.remove(min_v_line)
                            vertical_lines.append(v)
                    else:
                        vertical_lines.append(v)
        return vertical_lines
def initialize_word_variables(lines_table, j,i):
        vetical_x = []
        find_line = False
        word = lines_table[i][j][1][1]  # get the word 
        box = lines_table[i][j][1][2]   # the box or the coordinate of the word (x1 , y1 , x2 ,y2 )
        x2 = box[2]                     # the x2 of the word 
        y2=box[3]
        add_cell_word = False
        return vetical_x, find_line, word, box, x2, add_cell_word,y2

def process_word_comparison(box, next_box, vertical_lines, x1_word, x2):
        x1_next = next_box[0]
        y1_word = box[1]
        diff = x1_next - x2

        if vertical_lines:
            if len(vertical_lines) > 3:
                diff = x1_next - x2
            else:
                diff = x1_next - x2 - 5
        else:

            diff = x1_next - x2 - 5

        return x1_next, y1_word, diff
def change_diff_to_pos(diff,j):
        if j!=0:
            if diff<0:
                diff=0
        return diff


def process_diff_condition(diff, word, box, x1_word, x2,cell_word):
        if diff >= 0 and diff <= 6:
            cell_word += word + " "
            if box[0] < x1_word:   # box [0] x1 of box (word)
                x1_word = box[0]
            if box[2] > x2:
                x2 = box[2]

        return cell_word, x1_word, x2


def find_vertical_x(x1_x2_cord, x1_word, x2, vertical_lines):
        vertical_x = []
        nearst_line_before = None
        nearst_line_after = None
        line_before = None
        line_after = None
        for ind, line in enumerate(vertical_lines):
            x1_line = line[0]
            diff_before = x1_word - x1_line
            diff_after = x1_line - x2
            if ind == 0:
                nearst_line_before = diff_before
                line_before = x1_line
                nearst_line_after = diff_after
                line_after = x1_line
            else:
                if diff_before >= 0 and diff_before < nearst_line_before:
                    nearst_line_before = diff_before
                    line_before = x1_line

                if diff_after >= 0 and diff_after < nearst_line_after:
                    nearst_line_after = diff_after
                    line_after = x1_line

                elif diff_after >= 0 and nearst_line_after < 0:
                    nearst_line_after = diff_after
                    line_after = x1_line
        find_line = True 
        if line_before == line_after:
            line_before = x1_word
        vertical_x = [line_before, line_after]
        return vertical_x,find_line

def find_vertical_x_before_line(ind, find_line, vertical_lines, ind_before_line, x1_word):
        if ind > 0:
            while find_line == False:
                diff_line = vertical_lines[ind][2] - vertical_lines[ind_before_line][0]
                if diff_line >= 15 and vertical_lines[ind_before_line][0]-5 <= x1_word  :
                    find_line = True 
                else:
                    ind_before_line -= 1
            diff_x1_line_x2_line=vertical_lines[ind][2]-vertical_lines[ind_before_line][0]

            if diff_x1_line_x2_line >10:

                vertical_x = [vertical_lines[ind_before_line][0], vertical_lines[ind][0]]
            else :
                vertical_x = [vertical_lines[ind_before_line][0], vertical_lines[ind][2]]

        else:
            find_line = True 
            vertical_x = [x1_word, vertical_lines[ind][2]]

        return vertical_x, find_line


