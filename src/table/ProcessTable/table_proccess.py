import re
import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from table.ProcessTable.clean_header import clean_column_names
from table.ProcessTable.first_num import extract_first_number
from table.ProcessTable.fix_nan_rows import merge_nan_rows
from table.ProcessTable.smallest_num import extract_smallest_num, extract_smallest_number
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.numbers_modify import convert_amount_format_tab
from shared_files.reverse_text import reverse_arabic_words_tab
from shared_files.convert_arabic_numbers import convert_if_arabic
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code/InvoiceData')
#from process_img_text import invoice_info
    
def df_proccessing(df, tot, sub):
    #text_processor = invoice_info()  
    #df.replace({'\$': np.nan}, regex=True, inplace=True)
    # Clean column names
    df = clean_column_names(df)
    # Function to merge cells by adding values with a space and delete rows
    # Words to search for in any cell of the DataFrame
    ###############
    words_to_search = ['total', 'tax', 'vat', 'اجمالى','مستلم','المستلم', 'discount', 'subtotal','الخصومات','خصومات','المبلغ المستحق','المستحق الرصيد','المستحق المبلغ','المستحق','مبلغ المستحق','فاتوة','الفاتورة','invoice','الإجمالي','الاجمالي','amount','المضافة القيمة','الإجمالي قبل الضريبة','الضريبة قبل الإجمالي','المجموع','ضريبة','الخصم','القيمة المضافة']
    # Iterate over each row in the DataFrame, starting from the second row
    for index, row in df.iloc[1:].iterrows():
        # Check if any cell in the row contains the words
        if any(word in str(cell) for word in words_to_search for cell in row):
            # Truncate the DataFrame from this row onwards
            df = df.iloc[:index]  # Include the row where the words were found
            break
    # Define mapping for column names  #(?!^كود)المنتج
    column_mapping = {
        'quantity': ['كمية','كميه','الكمي', 'الكمي(ة|ه)','qty','quantity الكمية','qnty','الكمية quantity','qty الكمية','الكمية qty','كمية','quantity كمية','كمية quantity','qty كمية','كمية qty','العدد','العدد quantity','quantity العدد','qty العدد','العدد qty','quantities'],
        'unit price': ['price before vat', 'unit prices','السعر price','سعر الوحدة','الوحدة سعر' ,'الشعر','السعر','المسعر','المبلغ','uprice','u price','price','السعر','سعر','سعر price', 'rate','price سعر','unit price سعر الوحدة','سعر الوحدة unit price','price السعر','السعر price', 'prices','unit price الوحدة سعر','الوحدة سعر unit price','rate السعر','السعر rate','السعر الإفرادي unit price','unit price السعر الإفرادي',"الإفرادي سعر","الإفرادي سعر unit price",'unit price الإفرادي سعر','الافرادي','الإفرادي','السعر الإفرادي'],
        'total': ['line total','net','totals','item subtotal','ال(ا|إ|أ)جمال(ي|ى)','ال(إ|ا|أ)جمالي(ة|ه)','الإج','الإجمالي قبل الضريبة','الضريبة قبل الإجمالي','الضريبة الإجمالي قبل','total value','الكمية line سعر total','total الاجمالي','الاجمالي total','extension الإجمالي','الإجمالي extension','الصاف(ي|ى)','amount taxable','total before vat','total without vat','الاجمالي قبل الضريبة','السعر الاجمالي','الإجمالى قبل الضريبة','السعر الإجمالى','الإجمالي قبل الضريبة','السعر الإجمالي','amount(^vat)','amount','extension','total price السعر الإجمالي','السعر الإجمالي total price','total price إجمالي المبلغ','total price','إجمالي المبلغ','إجمالي المبلغ total price','total prices','السعر الإجمالي','الإجمالي','إجمالي', 'الإجمالى','subtotal','sub total','total إجمالي','إجمالي total','القيمة الإجمالية','القيمة','القيمة amount','amount القيمة','total إجمالى','إجمالى total','amount إجمالي','إجمالي amount','سعر الكمية','line total','سعر الكمية line total','line total سعر الكمية','total الإجمالي','الإجمالي total','total الإجمالى','الإجمالى total','القيمة الإجمالية','الإجمالية القيمة','المجموع','الإجمالية القيمة total amount','القيمة الإجمالية total amount','total amount القيمة الإجمالية','total amount الإجمالية القيمة','total amount',"سعر الكمية line amount",'line amount الكمية سعر',"سعر الكمية line amount",'line amount الكمية سعر','total المجموع','المجموع total'],
        'المنتج': ['iems','item','مان','ال','الب','البيا','البي','الصنف بيان','المنتجات','بيان الصنف','الخدمة نوع','إسم الصنف','(إ|ا|أ)سم الصنف','describtion','description','البيان','المنتج','ان','products','product', 'الوصف','الوص',"الصنف بيان",'بيان الصنف','description of goods',"بيان الصنف description",'description الصنف بيان','الصن',"بيان الصنف description",'description الصنف بيان','description of goods سم الوصف(إ|ا|أ)','description البيان','البيان description','description التفاصيل','التفاصيل description','التفاصيل','product المنتج','المنتج product','part','nature of goods','nature of goods or sevices','البند','nature of goods or sevices طبيعة البضائع و الخدمات','nature of goods or sevices تفاصيل السلع و الخدمات','الصنف (ا|إ|أ)سم','تفاصيل السلع و الخدمات nature of goods or sevices','تفاصيل السلع و الخدمات','طبيعة البضائع و الخدمات nature of goods or sevices','سم(إ|ا|أ) المادة','المادة (أ|إ|ا)سم','service type','service type نوع الخدمة','description (إ|ا|أ)سم القطعة', '(إ|ا|أ)سم القطعة description','سم القطعة(أ|إ|ا)','descr', 'البيـ','ــان'],
    }
    # Words to exclude from being mapped
    words_to_exclude = ['item no', 'item code', 'amount with vat','amount vat','net amount','part number','part no','amount inclusive of vat','tax amount','amount tax']
    # Iterate over column mapping and apply mapping, excluding certain words
    for common_name, alternative_names in column_mapping.items():
        for alt_name in alternative_names:
            # Construct regular expression pattern to exclude certain words
            pattern = r'\b(?!(?:' + '|'.join(words_to_exclude) + r')\b)' + alt_name + r'\b' # Negative lookahead assertion
            regex = re.compile(pattern, flags=re.IGNORECASE)
            matched_columns = [col for col in df.columns if regex.search(col)]
            if matched_columns:
                df.rename(columns={matched_columns[0]: common_name}, inplace=True)


    ########################
    # Find all columns containing 'total'
    # Find all columns containing 'total'
    #total_columns = [col for col in df.columns if 'total' in col.lower()]
    #total_columns = [col for col in df.columns if 'total' in col.lower() and re.match(r'^tax %.*amount$', col, flags=re.IGNORECASE) is None]    
    # Keep the column with the smallest sum if multiple 'total' columns are found
    #if len(total_columns) > 1:
    #    min_total_col = min(total_columns, key=lambda col: safe_sum(df[col]))
    duplicates = df.columns[df.columns.duplicated()]
    #print("Duplicated column names:", duplicates)
    # Merge the columns with duplicated names
    for dup_col in duplicates:
        #df = df.fillna('')
        dup_indices = [i for i, col in enumerate(df.columns) if col == dup_col]
        merged_col = df.iloc[:, dup_indices].apply(lambda x: ' '.join(x.dropna().astype(str)), axis=1)
        df.drop(columns=dup_col, inplace=True)
        df[dup_col] = merged_col
    # Define characters to replace
    # Apply the function to the DataFrame     
    #print(df['quantity'])
    if 'unit price' in df.columns:
        #df['unit price'] = df['unit price'].str.replace(',', '.')
        #df['unit price'] = df['unit price'].astype(str).str.replace('[^\d.-]', '', regex=True)
        #df['unit price'] = df['unit price'].apply(self.extract_first_number)
        #df['unit price'] = df['unit price'].astype(str).str.replace(':', ' ')
        df['unit price'] = df['unit price'].astype(str).apply(extract_first_number)
        #df['unit price'] = df['unit price'].astype(str).str.replace(':', ' ').apply(self.extract_first_number)
    if 'quantity' in df.columns:
        #df['quantity'] = df['quantity'].str.replace(',', '.')
        #df['quantity'] = df['quantity'].astype(str).str.replace('[^\d.-]', '', regex=True)    
        #df['quantity'] = df['quantity'].apply(self.extract_first_number)
        #df['quantity'] = df['quantity'].astype(str).str.replace(':', ' ')
        df['quantity'] = df['quantity'].astype(str).apply(extract_first_number)
        df['quantity'] = df['quantity'].astype(str)
        #df['quantity'] = df['quantity'].astype(str).str.replace(':', ' ').apply(self.extract_first_number)
    # Apply the function to the 'total' column
    if 'total' in df.columns and 'unit price' in df.columns:
        df['unit price'] = df['unit price'].astype(str)
        df['unit price'] = df['unit price'].str.replace(',', '.')
        #df['unit price'] = df['unit price'].str.replace('[^\d.]', '', regex=True)
        df['total'] = df.apply(lambda row: extract_smallest_number(row['total'], row['unit price']), axis=1)
    elif 'total' in df.columns:
        df['total'] = df['total'].apply(extract_smallest_num)
    # Call the function to merge NaN rows
    #df=merge_nan_rows(df1)
    #print(df['quantity'])
    #if df[['total', 'amount', 'unit price']].isnull().any(axis=1).any(): 
    if 'quantity' in df.columns:
        if df['quantity'].dtype == 'object':
            #if "," in df['quantity']:
            df['quantity'] = df['quantity'].str.replace(',', '.')  # Replace commas with periods                
            df['quantity'] = df['quantity'].str.replace('[^\d.]', '', regex=True)  # Remove non-numeric characters
            df['quantity'] = df['quantity'].apply(convert_if_arabic)
            df['quantity'] = df['quantity'].apply(convert_amount_format_tab)
        df['quantity'] = pd.to_numeric(df['quantity'], errors='coerce')
        
    if 'unit price' in df.columns:
        if df['unit price'].dtype == 'object':
            #if "," in df['unit price']:
            df['unit price'] = df['unit price'].str.replace(',', '.')  # Replace commas with periods 
            df['unit price'] = df['unit price'].str.replace('[^\d.]', '', regex=True)  # Remove non-numeric characters
            df['unit price'] = df['unit price'].apply(convert_if_arabic)
            df['unit price'] = df['unit price'].apply(convert_amount_format_tab)
        df['unit price'] = pd.to_numeric(df['unit price'], errors='coerce')
    if 'total' in df.columns:
        if df['total'].dtype == 'object':
            #if "," in df['total']:
            df['total'] = df['total'].str.replace(',', '.')  # Replace commas with periods                
            df['total'] = df['total'].str.replace('[^\d.]', '', regex=True)  # Remove non-numeric characters
            df['total'] = df['total'].apply(convert_if_arabic)
            #df['total'] = df['total'].apply(convert_amount_format_tab)
        df['total'] = pd.to_numeric(df['total'], errors='coerce')

    if 'total' in df.columns and 'quantity' in df.columns and 'unit price' in df.columns:
        df['total'] = df['total'].fillna((df['quantity'] * df['unit price']).round(2))
        df['quantity'] = df['quantity'].fillna((df['total'] / df['unit price']).round(2))
        df['unit price'] = df['unit price'].fillna((df['total'] / df['quantity']).round(2))

    if 'total' in df.columns and 'quantity' in df.columns and 'unit price' not in df.columns:
        df['unit price'] = (df['total'] / df['quantity']).round(2)
    if 'total' in df.columns and 'unit price' in df.columns and 'quantity' not in df.columns:
        df['quantity'] = (df['total'] / df['unit price']).round(2)
    if 'unit price' in df.columns and 'quantity' in df.columns and 'total' not in df.columns:
        df['total'] = (df['quantity'] * df['unit price']).round(2)
    # Call the function to handle merged column names
    df.replace('', np.nan, inplace=True)
    df = df.replace('nan', np.nan)
    df = df.dropna(axis=1, how='all')
    df = df.dropna(axis=0, how='all')
    # Check if 'unit price' column exists in the DataFrame columns
    #if df['unit price'].isnull().any():
    if 'unit price' not in df.columns and any(col in df.columns for col in ['unit', 'الوحدة', 'الوحدة unit', 'unit الوحدة']):
        # Iterate over each column and check if it contains numerical values
        for col in ['unit', 'الوحدة', 'الوحدة unit', 'unit الوحدة']:
            if col in df.columns and df[col].str.contains(r'\d').any():
                # Select the column with numerical values and rename it to 'unit price'
                df.rename(columns={col: 'unit price'}, inplace=True)
                # Further processing steps for the 'unit price' column
                if df['unit price'].dtype == 'object':
                    #if "," in df['unit price']:
                    df['unit price'] = df['unit price'].str.replace(',', '.')  # Replace commas with periods                           
                    df['unit price'] = df['unit price'].str.replace('[^\d.]', '', regex=True)  # Remove non-numeric characters  
                    df['unit price'] = df['unit price'].apply(convert_if_arabic)
                    df['unit price'] = df['unit price'].apply(convert_amount_format_tab)
                df['unit price'] = pd.to_numeric(df['unit price'], errors='coerce')
                if 'total' in df.columns and 'quantity' in df.columns and 'unit price' in df.columns:
                    df['total'] = df['total'].fillna((df['quantity'] * df['unit price']).round(2))
                    df['quantity'] = df['quantity'].fillna((df['total'] / df['unit price']).round(2))
                    df['unit price'] = df['unit price'].fillna((df['total'] / df['quantity']).round(2))
                if 'total' in df.columns and 'unit price' in df.columns and 'quantity' not in df.columns:
                    df['quantity'] = (df['total'] / df['unit price']).round(2)
                if 'unit price' in df.columns and 'quantity' in df.columns and 'total' not in df.columns:
                    df['total'] = (df['quantity'] * df['unit price']).round(2)
                break  # Exit the loop after the first column is found and processed
    #df = df.dropna(axis=0, how='all')
    # Check if totals variable is defined
    #total = 335.8  # Example value
    #subtotal = 335.8
    #df = df.fillna(' ')
    if 'total' in df.columns:
        total=tot
        subtotal=sub
        # Check if the sum of the 'total' column matches the value in the 'totals' variable
        if (df['total'].sum() == total) or (df['total'].sum() == subtotal):
            # Stop the DataFrame here and drop the rest of the rows
            #last_valid_index = df.index[df['total'].cumsum().ge(total)][0]
            last_valid_index = df.index[df['total'].cumsum().ge(total) | df['total'].cumsum().ge(subtotal)].max()
            df = df.iloc[:last_valid_index + 1]
                  
    ######
    # Iterate over each row in the DataFrame
    all_keys_and_values = []
    for key, value_list in column_mapping.items():
        all_keys_and_values.append(key)
        all_keys_and_values.extend(value_list)
    #print(all_keys_and_values)
    #for value in all_keys_and_values:
        #df = df[~df.isin([value]).any(axis=1)]
    #################
    words_to_exclude = ['الوص','البيا','ان','البي','الب','ال','الكمي','الإج','مان','الصن','البيـ','ــان']  # Words to exclude from dropping rows
    # Iterate over DataFrame cells and drop rows if any word is found
    indexes_to_drop = []  # Collect indexes to drop
    for index, row in df.iterrows():
        for col in df.columns:
            for word in all_keys_and_values:
                if word in str(row[col]).lower() and word not in words_to_exclude:
                    #print(f"Word '{word}' found in row {index}, column {col}. Dropping row.")
                    indexes_to_drop.append(index)
                    break  # Exit inner loop if any word is found to drop the row
            else:
                continue
            break  # Exit outer loop if any word is found to drop the row
    # Drop rows
    df = df.drop(indexes_to_drop)
    # Reset index
    df = df.reset_index(drop=True)
    df = merge_nan_rows(df)
    ####################
    df = df.dropna(axis=0, how='all')
    df_reversed = df.map(reverse_arabic_words_tab)
    ######################################
    exclude_columns = ['total', 'quantity','unit price']
    # Check if 'products' column is present in the DataFrame
    if 'المنتج' not in df_reversed.columns:
        max_length = 0
        max_column = None
        # Iterate through each column
        for col in df_reversed.columns:
            # Check if the column contains string values
            if df_reversed[col].dtype == 'object'  and col not in exclude_columns:
                # Check if the column contains only numeric values as strings
                if not re.match(r'^\s*\d+(\.\d+)?\s*$', df_reversed[col].iloc[0]):
                    # Find the maximum length of string in the column
                    col_max_length = df_reversed[col].apply(lambda x: len(str(x))).max()
                    # Update max_length and max_column if the current column has a longer string and length > 10
                    if col_max_length > max_length and col_max_length > 4:
                        max_length = col_max_length
                        max_column = col
        if max_column:
            # Rename the column with the longest string values to 'products'
            df_reversed.rename(columns={max_column: 'المنتج'}, inplace=True)
    # Replace empty strings with NaN
    df_reversed.replace('', np.nan, inplace=True)
    df_reversed = df_reversed.replace('nan', np.nan)
    df_reversed = df_reversed.dropna(axis=0, how='all')
    #df_reversed = df_reversed.fillna(0)
    #df_reversed = df_reversed.loc[(df != 0).any(axis=1)]
    ###########################################
    df_reversed.rename(columns={'quantity': 'الكمية', 'unit price': 'سعر الوحدة', 'total': 'السعر الإجمالي'}, inplace=True)
    # Define the columns you want to ensure exist
    required_columns = ['المنتج', 'الكمية', 'سعر الوحدة', 'السعر الإجمالي']
    # Add missing columns with NaN values
    for column in required_columns:
        if column not in df_reversed.columns:
            df_reversed[column] = np.nan
    selected_columns = ['السعر الإجمالي', 'سعر الوحدة', 'الكمية','المنتج']
    df_reversed = df_reversed[selected_columns]
    df_reversed.replace({'inf': np.nan}, regex=True, inplace=True)
    df_reversed = df_reversed.dropna(axis=0, how='all')
    df_reversed = df_reversed[~(df_reversed['المنتج'].notna() & df_reversed.drop('المنتج', axis=1).isna().all(axis=1))]
    # Replace 'nan' with ''
    #for index, row in df_reversed.iterrows():
     #   for col in df_reversed.columns:
     #       df_reversed.at[index, col] = str(row[col]).replace('nan', '')
    return df_reversed