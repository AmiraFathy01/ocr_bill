import re
import numpy as np
import pandas as pd
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.numbers_modify import convert_amount_format_tab
def extract_smallest_num(strings):
    if isinstance(strings, str):
        strings = strings.replace(',', '.')  # Replace commas with periods
        strings = re.sub(r'[^\d.]', ' ', strings)  # Remove non-numeric characters
        # Handle cases where the string is empty
        if not strings:
            return np.nan
        strings= convert_amount_format_tab(strings)
        #print("\n\ncode problem here is:\n",strings)
        numbers = [float(num) for num in strings.split() if num.strip() and num.strip() != '.']  # Added condition to skip empty strings
        if numbers:  # Check if numbers list is not empty
            return str(min(numbers))
        else:
            return np.nan
    elif np.isnan(strings):
        return np.nan
    else:
        return str(strings)

def extract_smallest_number(strings, unit_price):
    if isinstance(strings, str):
        strings = strings.replace(',', '.')  # Replace commas with periods
        strings = re.sub(r'[^\d.]', ' ', strings)  # Remove non-numeric characters
        # Handle cases where the string is empty
        if not strings:
            return np.nan
        strings = convert_amount_format_tab(strings)
        #print("\n\ncode problem here is:\n", strings)
        numbers = [float(num) for num in strings.split() if num.strip() and num.strip() != '.']  # Added condition to skip empty strings
        if numbers:  # Check if numbers list is not empty
            if unit_price and unit_price != 'nan':
                unit_price = float(unit_price)
                numbers = [num for num in numbers if num >= unit_price]
            if numbers:
                return str(min(numbers))
            else:
                return np.nan
        else:
            return np.nan
    elif np.isnan(strings):
        return np.nan
    else:
        return str(strings)       