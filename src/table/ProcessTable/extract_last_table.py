import re
#from io import BytesIO
import pandas as pd
import numpy as np
from google.api_core.exceptions import InternalServerError  # Import the InternalServerError exception
import warnings
warnings.filterwarnings('ignore')
from table.ProcessTable.table_proccess import df_proccessing
from table.ProcessTable.fix_gemini_output import last_process
from table.ProcessTable.extract_tab_gemini import missing_columns_gemini, extract_table_gemini
def process_excel_file(excel_file_path,text, tot, sub,GOOGLE_API_KEY):
    output_dict = []
    try:
        #df= excel_file_path
        df =  pd.read_excel(excel_file_path)
        if df is not None and not df.empty:
            #excel_file = BytesIO()
            # Write DataFrame to the BytesIO object as an Excel file
            #df.to_excel(excel_file, index=False)
            #excel_file.seek(0)
            #df =  pd.read_excel(excel_file_path)
            output = df_proccessing(df,tot, sub)
            processed_df = missing_columns_gemini(output, text,GOOGLE_API_KEY)
            #print(processed_df)
            #if processed_df is not None:
                #output_dict = processed_df.values.tolist()
        else:
            attempt = 1
            while attempt <= 5:
                processed_df = extract_table_gemini(text,GOOGLE_API_KEY)
                if processed_df is not None and len(processed_df.columns) == 4:
                    #print(processed_df)
                    #output_dict = processed_df.values.tolist()
                    break
                else:
                    processed_df = None
                attempt += 1
            else:
                processed_df = None
                print("نعتذر و لكن لم يتم العثور علي اي جداول لاستخراج البيانات منها")

        # Replace 'nan' with ''
        if processed_df is not None and processed_df.apply(lambda row: row.astype(str).str.contains('nan')).any().any():  # Check if processed_df is not None
            for index, row in processed_df.iterrows():
                for col in processed_df.columns:
                    processed_df.at[index, col] = str(row[col]).replace('nan', '')
            #print(processed_df)
            # Iterate through each column in the DataFrame
            #if processed_df is not None:
                #for column in processed_df.columns:
                # Convert the column to a list and store it in the dictionary
                    #output_dict[column] = processed_df[column].tolist()
                #print(output_dict)
            
        #print('------------------------------------------------------------------------------------\n')
        #print('\nFinal Results\n')
        if processed_df is not None:
            processed_df = last_process(processed_df)
            if processed_df is not None:
                output_dict = processed_df.values.tolist()
                #print(processed_df)
                #print(output_dict)
            else:
                output_dict = None
                processed_df= None
                #print(processed_df)
    except InternalServerError as e:
        processed_df = None
        output_dict = None
        print(f"An internal server error occurred: {e}")
        # Optionally, you can log the error or take other actions
    except Exception as ex:
        processed_df = None
        output_dict = None
        print(f"An unexpected error occurred: {ex}")
        # Optionally, you can log the error or take other actions

    return processed_df, output_dict  