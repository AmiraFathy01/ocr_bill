import re
import numpy as np
import pandas as pd
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.numbers_modify import convert_amount_format_tab

def extract_first_number(string):
    # Check if the input is not a string or is NaN
    if not isinstance(string, str) or pd.isna(string):
        return ''
    if isinstance(string, str):
        string = string.replace(',', '.')
        string = re.sub(r'[^\d.]', ' ', string)
        if not string:
            return np.nan
        string = convert_amount_format_tab(string)
        # Use regular expression to find the first number in the string
        match = re.search(r'\d+(\.\d+)?', string)
        if match:
            return str(match.group())  # Return the matched number
        else:
            return np.nan  # If no number found, return an empty string