import re
import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from table.ProcessTable.clean_header import clean_column_names
from shared_files.convert_arabic_numbers import convert_if_arabic

def check_and_rename(column_name):
    if re.search(r'(\b|\s)?اسعار الوحدات(\b|\s)?|(\b|\s)?سعر وحدة(\b|\s)?|(\b|\s)?(.)*سعر الوحدة(.)*(\b|\s)?', column_name):
        return 'سعر الوحدة'
    if re.search(r'(\b|\s)?كمية(\b|\s)?|(\b|\s)?كميات(\b|\s)?|(\b|\s)?الكمية(\b|\s)?', column_name):
        return 'الكمية'       
    if re.search(r'(\b|\s)?منتج(\b|\s)?|(\b|\s)?المنتجات(\b|\s)?|(\b|\s)?منتجات(\b|\s)?|(\b|\s)?المنتج(\b|\s)?', column_name):
        return 'المنتج'
    if re.search(r'(\b|\s)?(.)*السعر الإجمالي(.)*(\b|\s)?|(\b|\s)?سعر إجمالي(\b|\s)?|(\b|\s)?الإجمالي(\b|\s)?|(\b|\s)?إجمالي(\b|\s)?|(\b|\s)?الاجمالي(\b|\s)?|(\b|\s)?اجمالي(\b|\s)?|(\b|\s?)المجموع(\b|\s)?|(\b|\s)?السعر الإجمالي(\b|\s)?|(\b|\s)?السعر الاجمالي(\b|\s)?', column_name):
        return 'السعر الإجمالي'
    
    return column_name

       
def last_process(df):
    checker= None
    df = clean_column_names(df)
    # Iterate over DataFrame cells and drop rows if any word is found
    all_keys_and_values= ['سعر الوحدة','السعر الإجمالي','الكمية','المنتج']
    indexes_to_drop = []  # Collect indexes to drop
    for index, row in df.iterrows():
        for col in df.columns:
            for word in all_keys_and_values:
                if word in str(row[col]).lower():
                    print(f"Word '{word}' found in row {index}, column {col}. Dropping row.")
                    indexes_to_drop.append(index)
                    break  # Exit inner loop if any word is found to drop the row
            else:
                continue
            break  # Exit outer loop if any word is found to drop the row
    # Drop rows
    df = df.drop(indexes_to_drop)
    # Reset index
    df = df.reset_index(drop=True)
    special_characters = {'-','_', '*', '#',':'}
    #r'[\[\]\{\}\<\>\(\)\|~:;?!#]'
    # Iterate over each column
    for column in df.columns:
        # Remove special characters from column names
        new_column_name = column
        for char in special_characters:
            new_column_name = new_column_name.replace(char, '')
        # Update column name in the DataFrame
        df.rename(columns={column: new_column_name}, inplace=True)
        # Remove special characters from column values
        df[new_column_name] = df[new_column_name].apply(lambda x: ''.join([c for c in str(x) if c not in special_characters]))
    # if all row '' drop it
    df = df.drop(df[df.apply(lambda row: all(row == ''), axis=1)].index)
    df.rename(columns=check_and_rename, inplace=True) 
    if 'منتج' in df.columns:
        # Rename columns using the function
        #df.rename(columns={'منتج': 'المنتج'}, inplace=True)
        if any(any(df.columns.str.contains(word)) for word in ['المنتجات', 'منتجات', 'منتج']):
            df.rename(columns=lambda x: 'المنتج' if x in ['المنتجات', 'منتجات', 'منتج'] else x, inplace=True)
    if 'كمية' in df.columns:
        df.rename(columns={'كمية': 'الكمية'}, inplace=True)
    required_columns = ['المنتج', 'الكمية', 'سعر الوحدة', 'السعر الإجمالي']
    # Add missing columns with NaN values
    for column in required_columns:
        if column not in df.columns:
            df[column] = np.nan
    selected_columns = ['السعر الإجمالي', 'سعر الوحدة', 'الكمية','المنتج']
    df = df[selected_columns]
    df= df.fillna('')
    # if all row '' drop it
    df = df.drop(df[df.apply(lambda row: all(row == ''), axis=1)].index)
    df.replace('', np.nan, inplace=True)
    # Convert columns to string type if they are not already
    df['سعر الوحدة'] = df['سعر الوحدة'].astype(str).str.replace('[^\d.]', '', regex=True)
    df['السعر الإجمالي'] = df['السعر الإجمالي'].astype(str).str.replace('[^\d.]', '', regex=True)
    df['الكمية'] = df['الكمية'].astype(str).str.replace('[^\d.]', '', regex=True)
    df2= pd.DataFrame()
    # Ensure the column contains string values
    df2['المنتج'] = df['المنتج']
    df2['المنتج'] = df2['المنتج'].astype(str)
    df2['المنتج'] = df2['المنتج'].str.replace('[^\d.]', '', regex=True)  # Remove non-numeric characters 
    #df2['السعر الإجمالي'] = df['السعر الإجمالي'].str.replace('[^\d.]', '', regex=True)  # Remove non-numeric characters 
    #df2['الكمية'] = df['الكمية'].str.replace('[^\d.]', '', regex=True)  # Remove non-numeric characters 
    # Check if values in 'col2' contain only numerical values as strings
    if ((df['سعر الوحدة'] =='').all() and (df2['المنتج'] !='').all()) or ((df['السعر الإجمالي'] =='').all() and (df2['المنتج'] !='').all()) or ((df['الكمية'] =='').all() and (df2['المنتج'] !='').all()):
        df = None
    else:
        #df2= pd.DataFrame()
        df['سعر الوحدة'] = df['سعر الوحدة'].apply(convert_if_arabic)
        df['السعر الإجمالي'] = df['السعر الإجمالي'].apply(convert_if_arabic)
        df['الكمية'] = df['الكمية'].apply(convert_if_arabic)
        df['سعر الوحدة'] = pd.to_numeric(df['سعر الوحدة'], errors='coerce')
        df['السعر الإجمالي'] = pd.to_numeric(df['السعر الإجمالي'], errors='coerce')
        df['الكمية'] = pd.to_numeric(df['الكمية'], errors='coerce')
        #df['سعر الوحدة'] = df2['سعر الوحدة']
        #df['السعر الإجمالي'] = df2['السعر الإجمالي']
        #df['الكمية'] = df2['الكمية']
    if df is not None and not df.empty:
        for index, row in df.iterrows():
            for col in df.columns:
                df.at[index, col] = str(row[col]).replace('nan', '')
        df= df.fillna('')
    return df