import re
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
from google.api_core.exceptions import InternalServerError  # Import the InternalServerError exception
#import sys
#sys.path.append('C:/Users/RECOVERY WORLD/folders AI/code')
from shared_files.extract_with_gemini import fix_output_txt_gemini
from shared_files.reverse_text import reverse_arabic_words_tab

def missing_columns_gemini(df_reversed ,text,GOOGLE_API_KEY):
    #df_reversed = self.df_proccessing(df)
    #if ('المنتج' not in df_reversed.columns or 'السعر الإجمالي' not in df_reversed.columns or 'الكمية' not in df_reversed.columns or 'سعر الوحدة' not in df_reversed.columns or any(df_reversed[col].isna().all() for col in ['المنتج', 'السعر الإجمالي', 'الكمية', 'سعر الوحدة'])):
    # Check if the specified columns are in df_reversed
    #print("********************\n",df_reversed,"\n********************")
    #print(df_reversed.columns)
    missing_columns = [col for col in ['المنتج', 'السعر الإجمالي', 'الكمية', 'سعر الوحدة'] if col not in df_reversed.columns]
    #print("Missing Columns:", missing_columns)
    # Check if any of the specified columns are entirely NaN
    any_column_nan = any(df_reversed[col].isna().all() for col in ['المنتج', 'السعر الإجمالي', 'الكمية', 'سعر الوحدة'])
    #print("Any Column NaN:", any_column_nan)
    # Check if any condition fails
    if missing_columns or any_column_nan:
        try:
            ans = fix_output_txt_gemini(f"ما هى (المنتجات) و (سعر الوحدة) و (الكمية) و (السعر الإجمالي) لكل منتج بهذا النص:{text}  اخرج لى (المنتجات) و (سعر الوحدة) و (الكمية) و (السعر الإجمالي) فقط السليمة لكل منتج و قسم بينهم ب |",GOOGLE_API_KEY)
            # Extract the desired part of the text using regular expression
            match = re.search(r'\|(.*)\|', ans, re.DOTALL)
            if match:
                extracted_text = match.group(1).strip()
                lines = extracted_text.split('\n')
                # Extract headers
                headers = [header.strip() for header in lines[0].split('|') if header.strip()]
                # Initialize data dictionary
                data = {header: [] for header in headers}
                # Iterate through lines to extract values
                for line in lines[2:]:
                    values = [value.strip() for value in line.split('|') if value.strip()]
                    for header, value in zip(headers, values):
                            data[header].append(value)
                # Create a DataFrame
                df = pd.DataFrame(data)
                # Display the DataFrame
                df = df.map(reverse_arabic_words_tab)
                return df
            else:
                #print(df_reversed)
                return df_reversed
        except InternalServerError as e:
            print(f"An internal server error occurred: {e}")
            # Optionally, you can log the error or take other actions
        except Exception as ex:
            print(f"An unexpected error occurred: {ex}")
    else:
        #print(df_reversed)
        return df_reversed
###
def extract_table_gemini(text,GOOGLE_API_KEY):
    ans = fix_output_txt_gemini(f"ما هى (المنتجات) و (سعر الوحدة) و (الكمية) و (السعر الإجمالي) لكل منتج بهذا النص:{text}  اخرج لى (المنتجات) و (سعر الوحدة) و (الكمية) و (السعر الإجمالي) فقط السليمة لكل منتج و قسم بينهم ب |",GOOGLE_API_KEY)
    # Extract the desired part of the text using regular expression
    match = re.search(r'\|(.*)\|', ans, re.DOTALL)
    if match:
        extracted_text = match.group(1).strip()
        lines = extracted_text.split('\n')
        # Extract headers
        headers = [header.strip() for header in lines[0].split('|') if header.strip()]
        # Initialize data dictionary
        data = {header: [] for header in headers}
        # Iterate through lines to extract values
        for line in lines[2:]:
            values = [value.strip() for value in line.split('|') if value.strip()]
            for header, value in zip(headers, values):
                    data[header].append(value)
        # Create a DataFrame
        df = pd.DataFrame(data)
        df = df.map(reverse_arabic_words_tab)
        # Display the DataFrame
        #print(df)
        return df
