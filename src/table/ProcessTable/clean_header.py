import pandas as pd
# Function to remove unwanted characters from column names
def clean_column_names(df):
    # Dictionary to map unwanted characters to None (effectively removing them)
    mapping = {ord('.'): None,
               ord('('): None,
               ord(')'): None,
               ord('/'): None,
               ord('\\'): None,
               ord(':'): None,
               ord('*'): None,
               ord('-'): None,
               ord('_'): None,
              ord('#'): None}
    # Modify column names with translate
    df.columns = df.columns.str.strip().str.translate(mapping).str.lower()
    df.columns = df.columns.str.rstrip()
    return df