#!/usr/bin/env python
# coding: utf-8

# In[ ]:
## import library 
## import library 
import os,io 
import cv2 
import numpy as np 
from google.cloud import vision
from scipy.ndimage import interpolation as inter
import matplotlib.pyplot as plt 
from google.cloud import vision_v1 as vision
from google.cloud.vision_v1 import types
import pandas as pd 
from io import StringIO 
import re
import warnings 
warnings.filterwarnings('ignore') 
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

## import  Functions 
from OcrApi.DetectText import detect_text  
from OcrApi.FixImage import correct_skew 
## 
from text.SortY import sort_text_y
from text.FindDividingLine import find_dividing_line
from text.FindHorizontalLine import  find_horizontal_line
from text.FindLimitLine import find_limits_line
from text.SortX import  sort_line_x
##
from table.ExtractTable.GetBody import cell_df 
from table.ExtractTable.FindDescription import find_description,clean_text_table,text_tables_header_have_numbers
from table.ExtractTable.ProcessBody import add_cell_word_to_df,diff_between_line_x1_word,change_coordinate
from table.ExtractTable.FixHeader import fix_coord
from table.ExtractTable.GetHeader import header_df
from table.ExtractTable.ProcessHeaderBodyTable import extract_vertical_lines,initialize_word_variables,process_word_comparison
from table.ExtractTable.ProcessHeaderBodyTable import find_vertical_x,find_vertical_x_before_line,change_diff_to_pos,process_diff_condition
from table.ExtractTable.GetDataTable import get_data,get_text_table



class TextDetector:
    def __init__(self, service_account_path):
        try:
            self.client = vision.ImageAnnotatorClient.from_service_account_json(service_account_path)
            self.api = service_account_path
        except Exception as e:
            print(f"Error initializing Vision API client with service account path: {service_account_path}")
            print(f"Error message: {str(e)}")
            
            
    def get_text_df(self,image_path, word_header_eng, word_header_ara, word_header_merge):
        try :

            text_ocr_suc=None
            sorted_lines_scu=None
            text_scu=None  
            
        
            space_num = 15
            image_path = image_path
            try:
                # Attempt to process the image
                text_ocr, vertices_ocr = detect_text(image_path,self.api)
                text_ocr_suc=True
            except Exception as e:
                print(f"Error detecting text in image: {image_path}")
                print(f"Error message: {str(e)}")
                return None, None
        
            if text_ocr_suc==True:
                try:
                    best_angle,corrected_images = correct_skew(image_path)
                    image_path=corrected_images
                    sorted_lines = sort_line_x(text_ocr)
                    sorted_lines_scu=True

                except Exception as e:
                    print("Error sorting lines:")
                    print(f"Error message: {str(e)}")

                    text_alt=text_ocr[0].description
                    return text_alt, None
                
                if sorted_lines_scu==True:
                    try:
                        text, lines_table, limit_x1_x2, texts_table = get_data(image_path, sorted_lines, word_header_ara, word_header_eng, space_num)
                        text_scu=True

                    except Exception as e:
                        print("Error getting data:")
                        print(f"Error message: {str(e)}")
                        print("alternative text")
                        text_alt=text_ocr[0].description
                        return text_alt, None
                    if text_scu==True:

                        try:
                            lines_table, limit_x1_x2, texts_table, res = get_text_table(image_path, text, lines_table, limit_x1_x2, texts_table, sorted_lines, word_header_ara, word_header_eng, space_num)
                        except Exception as e:
                            print("Error getting text table:")
                            print(f"Error message: {str(e)}")
                            return text, None
                        
                        if not lines_table: # try again using th merg text 
                            try:
                                text, lines_table, limit_x1_x2, texts_table = get_data(image_path, sorted_lines, word_header_merge, word_header_eng, space_num)
                                text_scu=True
                            except Exception as e:
                                print("Error getting data:")
                                print(f"Error message: {str(e)}")
                                print("alternative text")
                                text_alt=text_ocr[0].description
                                return text_alt, None
                            
                            if text_scu==True:

                                try:
                                    lines_table, limit_x1_x2, texts_table, res = get_text_table(image_path, text, lines_table, limit_x1_x2, texts_table, sorted_lines, word_header_merge, word_header_eng, space_num)
                                except Exception as e:
                                    print("Error getting text table:")
                                    print(f"Error message: {str(e)}")
                                    return text, None
                        

                        try:
                            df, x1_x2_cord = header_df(lines_table, limit_x1_x2)
                        except Exception as e:
                            print("Error creating header DataFrame:")
                            print(f"Error message: {str(e)}")
                            return text, None
                        if df is not None and not df.empty:
                            while len(df.columns) < 4 and space_num < 22 :
                                try:
                                    lines_table, limit_x1_x2, texts_table, res = get_text_table(image_path, text, lines_table, limit_x1_x2, texts_table, sorted_lines, word_header_ara, word_header_eng, space_num, keywords=word_header_merge)
                                    df, x1_x2_cord = header_df(lines_table, limit_x1_x2)
                                    space_num += 1
                                    print("done")
                                except Exception as e:
                                    print("Error in loop while getting text table or creating header DataFrame:")
                                    print(f"Error message: {str(e)}")
                                    return text, None
                            try:
                                x1_x2_cord, df = fix_coord(x1_x2_cord, df)
                            except Exception as e:
                                print("Error fixing coordinates:")
                                print(f"Error message: {str(e)}")
                                return text, None
                            try:
                                df, x1_x2_cord = cell_df(df, lines_table, x1_x2_cord, limit_x1_x2)
                            except Exception as e:
                                print("Error creating cell DataFrame:")
                                print(f"Error message: {str(e)}")
                                return text, None
                            try:
                                # Extract the filename from the image path
                                #path_parts = image_path.split("\\")
                                #excel_filename = f'{path_parts[-1]}.xlsx'
                                # Save the DataFrame to an Excel file
                                #df.to_excel(excel_filename, index=False)
                                # with open(f'{path_parts[-1]}.txt', 'w', encoding='utf-8') as file:
                                    # Write the text data to the file
                                #    file.write(text)
                                #print(f"done :{path_parts}")
                                return text, df
                            except Exception as e:
                                print("Error saving to Excel or text file:")
                                print(f"Error message: {str(e)}")
                                return text, None
                        else :
                            return text,None
        except Exception as e:
            print("Error saving to Excel or text file:")
            print(f"Error message: {str(e)}")
            return None, None
        



