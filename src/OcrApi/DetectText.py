#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os,io 
import cv2 
import numpy as np 
from google.cloud import vision
from scipy.ndimage import interpolation as inter
import matplotlib.pyplot as plt 
from google.cloud import vision_v1 as vision
from google.cloud.vision_v1 import types
import pandas as pd 
from io import StringIO 
import re
import warnings 
warnings.filterwarnings('ignore') 
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

from OcrApi.FixImage import correct_skew 



def detect_text(image_path,api):
        """Detects text in the file."""
        try:
            client = vision.ImageAnnotatorClient.from_service_account_json(api)

            best_angle, corrected_image = correct_skew((image_path))

            _, encoded_image = cv2.imencode('.jpg', corrected_image)

            content = encoded_image.tobytes()

            image = vision.Image(content=content)
            response = client.text_detection(image=image)
            texts = response.text_annotations
            vertices = []
            texts_only = []
            for text in texts:
                v = text.bounding_poly.vertices
                vertice = [
                    f"({vertex.x},{vertex.y})" for vertex in text.bounding_poly.vertices
                ]
                vertices.append(vertice)

            if response.error.message:
                raise Exception(
                    "{}\nFor more info on error messages, check: "
                    "https://cloud.google.com/apis/design/errors".format(response.error.message)
                )
            return texts, vertices
        except Exception as e:
            try:
                """Detects text in the file."""

                client = vision.ImageAnnotatorClient.from_service_account_json(api)

                with open(image_path, "rb") as image_file:
                    content = image_file.read()
                    #print(type(content))

                image = vision.Image(content=content)
                response = client.text_detection(image=image)
                texts = response.text_annotations
                #print("Texts:")
                vertices =[]
                texts_only=[]
                for text in texts:
                    #print(f'\n"{text.description}"')
                    #texts_only.append(text.description)
                    v= text.bounding_poly.vertices
                    #print(v)
                    vertice = [
                        f"({vertex.x},{vertex.y})" for vertex in text.bounding_poly.vertices
                    ]
                    vertices.append(vertice)

                    #print("bounds: {}".format(",".join(vertice)))

                if response.error.message:
                    raise Exception(
                        "{}\nFor more info on error messages, check: "
                        "https://cloud.google.com/apis/design/errors".format(response.error.message)
                    )
                return texts ,vertices
            except Exception as e:
                print(f"Error detecting text in image: {image_path}")
                print(f"Error message: {str(e)}")
                return None, None

